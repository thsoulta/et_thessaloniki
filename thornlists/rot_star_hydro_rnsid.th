# This file was automatically generated using the GetComponents script.

!CRL_VERSION = 1.0


!DEFINE ROOT = Cactus
!DEFINE ARR  = $ROOT/arrangements
!DEFINE COMPONENTLIST_TARGET = $ROOT/thornlists/

!DEFINE ET_RELEASE = ET_2018_02


# A note on CUDA and OpenCL:

# The thorns supporting and using CUDA and OpenCL are working and are
# part of the Einstein Toolkit. However, these thorns are currently
# commented out in this thorn list because most machines do not have
# CUDA or OpenCL libraries installed, and there are no good, free CUDA
# or OpenCL implementations yet which could be shipped with Cactus.

# If you want to experiment with CUDA or OpenCL and use these thorns
# (or look at their documentation), then uncomment these thorns in
# this thorn list, and use GetComponents to download these thorns.

# You have to uncomment these thorns (not here, but further below):
#   - CactusExamples/HelloWorldCUDA
#   - CactusExamples/HelloWorldOpenCL
#   - CactusExamples/WaveToyOpenCL
#   - CactusUtils/OpenCLRunTime
#   - CactusUtils/Accelerator
#   - ExternalLibraries/OpenCL
#   - McLachlan/ML_WaveToy_CL

# To download these thorns via GetComponents, use then the following
# command (on the same system where you originally used
# GetComponents):

#   cd Cactus (or whatever your source tree is called)
#   bin/GetComponents --update --root=. manifest/einsteintoolkit.th



# This thorn list
!TARGET   = $ROOT
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/manifest.git
!REPO_PATH= $1
!NAME     = manifest
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = ./manifest

# Cactus Flesh
!TARGET   = $ROOT
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactus.git
!NAME     = flesh
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = .clang-format CONTRIBUTORS COPYRIGHT doc lib Makefile src

# Simulation Factory
!TARGET   = $ROOT
!TYPE     = git
!URL      = https://bitbucket.org/simfactory/simfactory2.git
!REPO_PATH=$1
!NAME     = simfactory2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = ./simfactory

# Example parameter files
!TARGET   = $ROOT
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteinexamples.git
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = par

# Various Cactus utilities
!TARGET   = $ROOT
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/utilities.git
!REPO_PATH= $1
!NAME     = utils
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = ./utils

# The GetComponents script
!TARGET   = $ROOT/bin
!TYPE     = git
!URL      = https://github.com/gridaphobe/CRL.git
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = GetComponents



# CactusBase thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactusbase.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
CactusBase/Boundary
CactusBase/CartGrid3D
CactusBase/CoordBase
CactusBase/Fortran
CactusBase/InitBase
CactusBase/IOASCII
CactusBase/IOBasic
CactusBase/IOUtil
CactusBase/SymBase
CactusBase/Time

# CactusConnect thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactusconnect.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
CactusConnect/HTTPD
CactusConnect/HTTPDExtra
CactusConnect/Socket

# CactusDoc thorns
!TARGET   = $ARR/CactusDoc
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/coredoc.git
!NAME     = CoreDoc
!REPO_PATH= $1
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = ./CoreDoc

# CactusElliptic thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactuselliptic.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = CactusElliptic/EllPETSc CactusElliptic/TATPETSc
CactusElliptic/EllBase
#DISABLED CactusElliptic/EllPETSc
CactusElliptic/EllSOR
CactusElliptic/TATelliptic
#DISABLED CactusElliptic/TATPETSc

# CactusExamples thorns
#--TS--#!TARGET   = $ARR
#--TS--#!TYPE     = git
#--TS--#!URL      = https://bitbucket.org/cactuscode/cactusexamples.git
#--TS--#!REPO_PATH= $2
#--TS--#!REPO_BRANCH = $ET_RELEASE
#--TS--#!CHECKOUT = CactusExamples/HelloWorldCUDA CactusExamples/HelloWorldOpenCL CactusExamples/WaveToyOpenCL
#--TS--#CactusExamples/DemoInterp
#--TS--#CactusExamples/FleshInfo
#--TS--#CactusExamples/HelloWorld
#DISABLED CactusExamples/HelloWorldCUDA
#DISABLED CactusExamples/HelloWorldOpenCL
#--TS--#CactusExamples/IDWaveMoL
#--TS--#CactusExamples/Poisson
#--TS--#CactusExamples/SampleBoundary
#--TS--#CactusExamples/SampleIO
#--TS--#CactusExamples/TimerInfo
#--TS--#CactusExamples/WaveMoL
#--TS--#CactusExamples/WaveToy1DF77
#--TS--#CactusExamples/WaveToy2DF77
#DISABLED CactusExamples/WaveToyOpenCL

#--TS--## CactusIO thorns
#--TS--#!TARGET   = $ARR
#--TS--#!TYPE     = git
#--TS--#!URL      = https://bitbucket.org/cactuscode/cactusio.git
#--TS--#!REPO_PATH= $2
#--TS--#!REPO_BRANCH = $ET_RELEASE
#--TS--#!CHECKOUT =
#--TS--#CactusIO/IOJpeg

# CactusNumerical thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactusnumerical.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
CactusNumerical/Cartoon2D
CactusNumerical/Dissipation
CactusNumerical/InterpToArray
CactusNumerical/LocalInterp
CactusNumerical/LocalInterp2
CactusNumerical/LocalReduce
CactusNumerical/MoL
CactusNumerical/Noise
CactusNumerical/Norms
CactusNumerical/Periodic
CactusNumerical/ReflectionSymmetry
CactusNumerical/RotatingSymmetry180
CactusNumerical/RotatingSymmetry90
CactusNumerical/Slab
CactusNumerical/SlabTest
CactusNumerical/SpaceMask
CactusNumerical/SphericalSurface
CactusNumerical/SummationByParts
CactusNumerical/TensorTypes
CactusNumerical/TestLocalInterp2
CactusNumerical/TestLocalReduce

# CactusPUGH thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactuspugh.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
CactusPUGH/PUGH
CactusPUGH/PUGHInterp
CactusPUGH/PUGHReduce
CactusPUGH/PUGHSlab

# CactusPUGHIO thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactuspughio.git
!REPO_PATH= $2
!CHECKOUT =
CactusPUGHIO/IOHDF5
CactusPUGHIO/IOHDF5Util

# CactusTest thorns
#--TS--#!TARGET   = $ARR
#--TS--#!TYPE     = git
#--TS--#!URL      = https://bitbucket.org/cactuscode/cactustest.git
#--TS--#!REPO_PATH= $2
#--TS--#!REPO_BRANCH = $ET_RELEASE
#--TS--#!CHECKOUT = CactusTest/TestAllTypes
#DISABLED CactusTest/TestAllTypes
#--TS--#CactusTest/TestArrays
#--TS--#CactusTest/TestComplex
#--TS--#CactusTest/TestCoordinates
#--TS--#CactusTest/TestFortranCrayPointers
#--TS--#CactusTest/TestFortranDependencies1
#--TS--#CactusTest/TestFortranDependencies2
#--TS--#CactusTest/TestFpointerNULL
#--TS--#CactusTest/TestFreeF90
#--TS--#CactusTest/TestGlobalReduce
#--TS--#CactusTest/TestInclude1
#--TS--#CactusTest/TestInclude2
#--TS--#CactusTest/TestLoop
#--TS--#CactusTest/TestMath
#--TS--#CactusTest/TestMoL
#--TS--#CactusTest/TestPar
#--TS--#CactusTest/TestReduce
#--TS--#CactusTest/TestSchedule
#--TS--#CactusTest/TestStrings
#--TS--#CactusTest/TestTable
#--TS--#CactusTest/TestTimers
#--TS--#CactusTest/TestTypes

# CactusUtils thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactusutils.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = CactusUtils/Accelerator CactusUtils/OpenCLRunTime
#DISABLED CactusUtils/Accelerator
CactusUtils/Formaline
CactusUtils/MemSpeed
CactusUtils/NaNCatcher
CactusUtils/NaNChecker
CactusUtils/Nice
CactusUtils/NoMPI
#DISABLED CactusUtils/OpenCLRunTime
CactusUtils/SystemStatistics
CactusUtils/SystemTopology
CactusUtils/TerminationTrigger
CactusUtils/TimerReport
CactusUtils/Trigger
CactusUtils/Vectors
CactusUtils/WatchDog

# CactusWave thorns
#--TS--#!TARGET   = $ARR
#--TS--#!TYPE     = git
#--TS--#!URL      = https://bitbucket.org/cactuscode/cactuswave.git
#--TS--#!REPO_PATH= $2
#--TS--#!REPO_BRANCH = $ET_RELEASE
#--TS--#!CHECKOUT =
#--TS--#CactusWave/IDScalarWave
#--TS--#CactusWave/IDScalarWaveC
#--TS--#CactusWave/IDScalarWaveCXX
#--TS--#CactusWave/IDScalarWaveElliptic
#--TS--#CactusWave/WaveBinarySource
#--TS--#CactusWave/WaveToyC
#--TS--#CactusWave/WaveToyCXX
#--TS--#CactusWave/WaveToyExtra
#--TS--#CactusWave/WaveToyF77
#--TS--#CactusWave/WaveToyF90
#--TS--#CactusWave/WaveToyFreeF90



# EinsteinAnalysis
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteinanalysis.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
EinsteinAnalysis/ADMAnalysis
EinsteinAnalysis/ADMMass
EinsteinAnalysis/AHFinder
EinsteinAnalysis/AHFinderDirect
EinsteinAnalysis/CalcK
EinsteinAnalysis/EHFinder
EinsteinAnalysis/Extract
EinsteinAnalysis/Hydro_Analysis
EinsteinAnalysis/Multipole
EinsteinAnalysis/Outflow
EinsteinAnalysis/PunctureTracker
EinsteinAnalysis/QuasiLocalMeasures
EinsteinAnalysis/WeylScal4

# EinsteinBase
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteinbase.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
EinsteinBase/ADMBase
EinsteinBase/ADMCoupling        # deprecated
EinsteinBase/ADMMacros          # deprecated
EinsteinBase/Constants
EinsteinBase/CoordGauge
EinsteinBase/EOS_Base
EinsteinBase/HydroBase
EinsteinBase/StaticConformal
EinsteinBase/TmunuBase

# EinsteinEOS
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteineos.git
!REPO_PATH= $2
!CHECKOUT =
EinsteinEOS/EOS_Hybrid
EinsteinEOS/EOS_IdealFluid
EinsteinEOS/EOS_Omni
EinsteinEOS/EOS_Polytrope

# EinsteinEvolve
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteinevolve.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
EinsteinEvolve/GRHydro
EinsteinEvolve/GRHydro_InitData
EinsteinEvolve/NewRad

# EinsteinInitialData
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteininitialdata.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
EinsteinInitialData/DistortedBHIVP
EinsteinInitialData/Exact
EinsteinInitialData/Hydro_InitExcision
EinsteinInitialData/Hydro_RNSID
EinsteinInitialData/IDAnalyticBH
EinsteinInitialData/IDAxiBrillBH
EinsteinInitialData/IDAxiOddBrillBH
EinsteinInitialData/IDBrillData
EinsteinInitialData/IDConstraintViolate
EinsteinInitialData/IDFileADM
EinsteinInitialData/IDLinearWaves
EinsteinInitialData/Meudon_Bin_BH
EinsteinInitialData/Meudon_Bin_NS
EinsteinInitialData/Meudon_Mag_NS
EinsteinInitialData/NoExcision
EinsteinInitialData/RotatingDBHIVP
EinsteinInitialData/TOVSolver
EinsteinInitialData/TwoPunctures

# EinsteinUtils
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteinutils.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
EinsteinUtils/SetMask_SphericalSurface
EinsteinUtils/TGRtensor



# Carpet, the AMR driver
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/eschnett/carpet.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = Carpet/doc
Carpet/Carpet
Carpet/CarpetEvolutionMask
Carpet/CarpetIOASCII
Carpet/CarpetIOBasic
Carpet/CarpetIOHDF5
Carpet/CarpetIOScalar
Carpet/CarpetIntegrateTest
Carpet/CarpetInterp
Carpet/CarpetInterp2
Carpet/CarpetLib
Carpet/CarpetMask
Carpet/CarpetProlongateTest
Carpet/CarpetReduce
Carpet/CarpetRegrid
Carpet/CarpetRegrid2
Carpet/CarpetRegridTest
Carpet/CarpetSlab
Carpet/CarpetTracker
Carpet/CycleClock
Carpet/HighOrderWaveTest
Carpet/LoopControl
Carpet/PeriodicCarpet
Carpet/ReductionTest
Carpet/ReductionTest2
Carpet/ReductionTest3
Carpet/RegridSyncTest
Carpet/TestCarpetGridInfo
Carpet/TestLoopControl
Carpet/Timers



# Thorns developed at the University of Catania
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/eloisa/ctthorns.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
CTThorns/CT_Analytic
CTThorns/CT_MultiLevel



!TARGET   = $ARR
!TYPE     = git
!URL      = https://github.com/barrywardell/EinsteinExact.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = EinsteinExact/doc EinsteinExact/m EinsteinExact/tests
EinsteinExact/EinsteinExact_Test
EinsteinExact/GaugeWave
EinsteinExact/KerrSchild
EinsteinExact/Minkowski
EinsteinExact/ModifiedSchwarzschildBL
EinsteinExact/ShiftedGaugeWave
EinsteinExact/Vaidya2



# Additional Cactus thorns
!TARGET   = $ARR
!TYPE     = svn
!URL      = https://svn.cactuscode.org/projects/$1/$2/branches/$ET_RELEASE
!CHECKOUT = ExternalLibraries/OpenBLAS ExternalLibraries/OpenCL ExternalLibraries/pciutils ExternalLibraries/PETSc
ExternalLibraries/BLAS
ExternalLibraries/FFTW3
ExternalLibraries/GSL
ExternalLibraries/HDF5
ExternalLibraries/hwloc
ExternalLibraries/LAPACK
ExternalLibraries/libjpeg
ExternalLibraries/LORENE
ExternalLibraries/MPI
#DISABLED ExternalLibraries/OpenBLAS
#DISABLED ExternalLibraries/OpenCL
ExternalLibraries/OpenSSL
ExternalLibraries/PAPI
#DISABLED ExternalLibraries/pciutils
#DISABLED ExternalLibraries/PETSc
ExternalLibraries/pthreads
ExternalLibraries/zlib

# A newer version of Lorene, by default disabled
!TARGET   = $ARR/ExternalLibraries
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/lorene.git
!NAME     = LORENE2
!REPO_BRANCH = $ET_RELEASE
!REPO_PATH= ../
!CHECKOUT = LORENE2
#DISABLED LORENE2


# From Kranc (required e.g. by McLachlan)
!TARGET   = $ARR
!TYPE     = git
!URL      = https://github.com/ianhinder/Kranc.git
!REPO_PATH= Auxiliary/Cactus
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
KrancNumericalTools/GenericFD



# McLachlan, the spacetime code
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/mclachlan.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = McLachlan/doc McLachlan/m McLachlan/par McLachlan/ML_WaveToy_CL
McLachlan/ML_ADMConstraints
McLachlan/ML_ADMQuantities
McLachlan/ML_BSSN
McLachlan/ML_BSSN_Helper
McLachlan/ML_BSSN_Test
McLachlan/ML_CCZ4
McLachlan/ML_CCZ4_Helper
McLachlan/ML_CCZ4_Test
McLachlan/ML_WaveToy
#DISABLED McLachlan/ML_WaveToy_CL
McLachlan/ML_WaveToy_Test



# Numerical
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/numerical.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
Numerical/AEILocalInterp



# PITTNullCode
#--TS--#!TARGET   = $ARR
#--TS--#!TYPE     = git
#--TS--#!URL      = https://bitbucket.org/einsteintoolkit/pittnullcode.git
#--TS--#!REPO_PATH= $2
#--TS--#!REPO_BRANCH = $ET_RELEASE
#--TS--#!CHECKOUT =
#--TS--#PITTNullCode/NullConstr
#--TS--#PITTNullCode/NullDecomp
#--TS--#PITTNullCode/NullEvolve
#--TS--#PITTNullCode/NullExact
#--TS--#PITTNullCode/NullGrid
#--TS--#PITTNullCode/NullInterp
#--TS--#PITTNullCode/NullNews
#--TS--#PITTNullCode/NullPsiInt
#--TS--#PITTNullCode/NullSHRExtract
#--TS--#PITTNullCode/NullVars
#--TS--#PITTNullCode/SphericalHarmonicDecomp
#--TS--#PITTNullCode/SphericalHarmonicRecon
#--TS--#PITTNullCode/SphericalHarmonicReconGen



# Various thorns from WVU
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/zach_etienne/wvuthorns.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
WVUThorns/IllinoisGRMHD
WVUThorns/Convert_to_HydroBase
WVUThorns/ID_converter_ILGRMHD
WVUThorns/Seed_Magnetic_Fields

# Llama
!TARGET   = $ARR
!TYPE     = git
!AUTH_URL = https://bitbucket.org/llamacode/llama.git
!URL      = https://bitbucket.org/llamacode/llama.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
Llama/ADMDerivatives
Llama/Coordinates
Llama/CoordinatesSymmetry
Llama/GlobalDerivative
Llama/Interpolate2
Llama/LlamaWaveToy
Llama/WaveExtractL


# This is an example on how to add your own thorns. First, you must checkout them from your repository 
# (such as a repository in bitbucket) and then you define your ArrangementName file (HITS_AUTH in my case).
# I use modified versions of EOS_Omni, and GRHydro (do not forget to comment the corresponding versions
# of ET in order to replace them with your versions).


# The HITS_AUTH Thorns
#--TS--# !TARGET   = $ARR
#--TS--# !TYPE     = git
#--TS--# !URL      = https://thsoulta@bitbucket.org/thsoulta/hits_auth_thorns.git
#--TS--# !REPO_PATH= $2
#--TS--# !CHECKOUT =
#--TS--# HITS_AUTH/EOS_Omni
#--TS--# HITS_AUTH/GRHydro

# Do not forget to actually checkout your thorns using:
# ./GetComponents --parallel your_thornlist.th