0    step
0    abscidia of the rotation axis [km] 
2005.506399283629    Orbital frequency Omega [rad/s] 
0.0502787626538348    Abscidia ``center of mass'' star 1 [km] 
0.0502787626538348    Abscidia ``center of mass'' star 2 [km] 
1.720185431770235   Baryon mass of star 1  [M_sol] 
0.01887756462883237   relative variation enth. star 1
11.34642665141636   R(theta=0) [km] 
11.78002990994011   R(theta=pi/2, phi=0) [km] 
11.12912785196306   R(theta=pi/2, phi=pi/2) [km] 
11.25548540716142   R(theta=pi/2, phi=pi) [km] 
1.720185431770235   Baryon mass of star 2  [M_sol] 
0.01887756462883237   relative variation enth. star 2
11.34642665141636   R(theta=0) [km] 
11.78002990994011   R(theta=pi/2, phi=0) [km] 
11.12912785196306   R(theta=pi/2, phi=pi/2) [km] 
11.25548540716142   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
1    step
0    abscidia of the rotation axis [km] 
2150.885831509609    Orbital frequency Omega [rad/s] 
0.1669946805054878    Abscidia ``center of mass'' star 1 [km] 
0.1669946805054878    Abscidia ``center of mass'' star 2 [km] 
1.524183402880695   Baryon mass of star 1  [M_sol] 
0.005585478372298386   relative variation enth. star 1
10.84621609811922   R(theta=0) [km] 
11.14573527768714   R(theta=pi/2, phi=0) [km] 
10.70037251216183   R(theta=pi/2, phi=pi/2) [km] 
10.95135213120261   R(theta=pi/2, phi=pi) [km] 
1.524183402880695   Baryon mass of star 2  [M_sol] 
0.005585478372298386   relative variation enth. star 2
10.84621609811922   R(theta=0) [km] 
11.14573527768714   R(theta=pi/2, phi=0) [km] 
10.70037251216183   R(theta=pi/2, phi=pi/2) [km] 
10.95135213120261   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
2    step
0    abscidia of the rotation axis [km] 
2232.794333841471    Orbital frequency Omega [rad/s] 
0.09930725048036493    Abscidia ``center of mass'' star 1 [km] 
0.09930725048036493    Abscidia ``center of mass'' star 2 [km] 
1.526882465066366   Baryon mass of star 1  [M_sol] 
0.002310307620047816   relative variation enth. star 1
10.80004206164668   R(theta=0) [km] 
11.08810124010031   R(theta=pi/2, phi=0) [km] 
10.69681531467623   R(theta=pi/2, phi=pi/2) [km] 
11.15230464867046   R(theta=pi/2, phi=pi) [km] 
1.526882465066366   Baryon mass of star 2  [M_sol] 
0.002310307620047816   relative variation enth. star 2
10.80004206164668   R(theta=0) [km] 
11.08810124010031   R(theta=pi/2, phi=0) [km] 
10.69681531467623   R(theta=pi/2, phi=pi/2) [km] 
11.15230464867046   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
3    step
0    abscidia of the rotation axis [km] 
2189.305394637699    Orbital frequency Omega [rad/s] 
0.02172390864216167    Abscidia ``center of mass'' star 1 [km] 
0.02172390864216167    Abscidia ``center of mass'' star 2 [km] 
1.542297284146132   Baryon mass of star 1  [M_sol] 
0.004749831376091081   relative variation enth. star 1
10.81157538979308   R(theta=0) [km] 
11.10671410494933   R(theta=pi/2, phi=0) [km] 
10.74816610593161   R(theta=pi/2, phi=pi/2) [km] 
11.30931728641539   R(theta=pi/2, phi=pi) [km] 
1.542297284146132   Baryon mass of star 2  [M_sol] 
0.004749831376091081   relative variation enth. star 2
10.81157538979308   R(theta=0) [km] 
11.10671410494933   R(theta=pi/2, phi=0) [km] 
10.74816610593161   R(theta=pi/2, phi=pi/2) [km] 
11.30931728641539   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
4    step
0    abscidia of the rotation axis [km] 
2404.421176919094    Orbital frequency Omega [rad/s] 
-0.01621932337738219    Abscidia ``center of mass'' star 1 [km] 
-0.01621932337738219    Abscidia ``center of mass'' star 2 [km] 
1.584536243014387   Baryon mass of star 1  [M_sol] 
0.008825871494046338   relative variation enth. star 1
10.69932090361485   R(theta=0) [km] 
11.36688393434874   R(theta=pi/2, phi=0) [km] 
10.60253180695761   R(theta=pi/2, phi=pi/2) [km] 
10.89866109992355   R(theta=pi/2, phi=pi) [km] 
1.584536243014387   Baryon mass of star 2  [M_sol] 
0.008825871494046338   relative variation enth. star 2
10.69932090361485   R(theta=0) [km] 
11.36688393434874   R(theta=pi/2, phi=0) [km] 
10.60253180695761   R(theta=pi/2, phi=pi/2) [km] 
10.89866109992355   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
5    step
0    abscidia of the rotation axis [km] 
2463.123808275102    Orbital frequency Omega [rad/s] 
0.1615910797105391    Abscidia ``center of mass'' star 1 [km] 
0.1615910797105391    Abscidia ``center of mass'' star 2 [km] 
1.566593624078793   Baryon mass of star 1  [M_sol] 
0.001774903837081385   relative variation enth. star 1
10.64255171354599   R(theta=0) [km] 
11.12851058253617   R(theta=pi/2, phi=0) [km] 
10.56832147097499   R(theta=pi/2, phi=pi/2) [km] 
11.04508327758606   R(theta=pi/2, phi=pi) [km] 
1.566593624078793   Baryon mass of star 2  [M_sol] 
0.001774903837081385   relative variation enth. star 2
10.64255171354599   R(theta=0) [km] 
11.12851058253617   R(theta=pi/2, phi=0) [km] 
10.56832147097499   R(theta=pi/2, phi=pi/2) [km] 
11.04508327758606   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
6    step
0    abscidia of the rotation axis [km] 
2486.946192174065    Orbital frequency Omega [rad/s] 
0.05562706507646231    Abscidia ``center of mass'' star 1 [km] 
0.05562706507646231    Abscidia ``center of mass'' star 2 [km] 
1.584266277226447   Baryon mass of star 1  [M_sol] 
0.002796150993697437   relative variation enth. star 1
10.67529664486217   R(theta=0) [km] 
11.11280102056684   R(theta=pi/2, phi=0) [km] 
10.61651418350781   R(theta=pi/2, phi=pi/2) [km] 
11.19355940989301   R(theta=pi/2, phi=pi) [km] 
1.584266277226447   Baryon mass of star 2  [M_sol] 
0.002796150993697437   relative variation enth. star 2
10.67529664486217   R(theta=0) [km] 
11.11280102056684   R(theta=pi/2, phi=0) [km] 
10.61651418350781   R(theta=pi/2, phi=pi/2) [km] 
11.19355940989301   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
7    step
0    abscidia of the rotation axis [km] 
2454.920595034138    Orbital frequency Omega [rad/s] 
0.004909069406942024    Abscidia ``center of mass'' star 1 [km] 
0.004909069406942024    Abscidia ``center of mass'' star 2 [km] 
1.600091707261127   Baryon mass of star 1  [M_sol] 
0.004427976499522741   relative variation enth. star 1
10.7105183185262   R(theta=0) [km] 
11.16436438225525   R(theta=pi/2, phi=0) [km] 
10.6687965258929   R(theta=pi/2, phi=pi/2) [km] 
11.27786682860451   R(theta=pi/2, phi=pi) [km] 
1.600091707261127   Baryon mass of star 2  [M_sol] 
0.004427976499522741   relative variation enth. star 2
10.7105183185262   R(theta=0) [km] 
11.16436438225525   R(theta=pi/2, phi=0) [km] 
10.6687965258929   R(theta=pi/2, phi=pi/2) [km] 
11.27786682860451   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
8    step
0    abscidia of the rotation axis [km] 
2152.594707468604    Orbital frequency Omega [rad/s] 
-0.01317853007690495    Abscidia ``center of mass'' star 1 [km] 
-0.01317853007690495    Abscidia ``center of mass'' star 2 [km] 
1.555468684923049   Baryon mass of star 1  [M_sol] 
0.002581112756611271   relative variation enth. star 1
10.62901218720098   R(theta=0) [km] 
11.02108012092375   R(theta=pi/2, phi=0) [km] 
10.65417643333264   R(theta=pi/2, phi=pi/2) [km] 
11.39670622473229   R(theta=pi/2, phi=pi) [km] 
1.555468684923049   Baryon mass of star 2  [M_sol] 
0.002581112756611271   relative variation enth. star 2
10.62901218720098   R(theta=0) [km] 
11.02108012092375   R(theta=pi/2, phi=0) [km] 
10.65417643333264   R(theta=pi/2, phi=pi/2) [km] 
11.39670622473229   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
9    step
0    abscidia of the rotation axis [km] 
2046.543141405056    Orbital frequency Omega [rad/s] 
-0.09911421575642176    Abscidia ``center of mass'' star 1 [km] 
-0.09911421575642176    Abscidia ``center of mass'' star 2 [km] 
1.550641442802037   Baryon mass of star 1  [M_sol] 
0.003641973132878444   relative variation enth. star 1
10.60460344537999   R(theta=0) [km] 
11.21583224891749   R(theta=pi/2, phi=0) [km] 
10.62708432099655   R(theta=pi/2, phi=pi/2) [km] 
11.22901861825656   R(theta=pi/2, phi=pi) [km] 
1.550641442802037   Baryon mass of star 2  [M_sol] 
0.003641973132878444   relative variation enth. star 2
10.60460344537999   R(theta=0) [km] 
11.21583224891749   R(theta=pi/2, phi=0) [km] 
10.62708432099655   R(theta=pi/2, phi=pi/2) [km] 
11.22901861825656   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
10    step
0    abscidia of the rotation axis [km] 
2029.210789807883    Orbital frequency Omega [rad/s] 
-0.005695462464649914    Abscidia ``center of mass'' star 1 [km] 
-0.005695462464649914    Abscidia ``center of mass'' star 2 [km] 
1.58886566439251   Baryon mass of star 1  [M_sol] 
0.003041722311099634   relative variation enth. star 1
10.71157954289784   R(theta=0) [km] 
11.39143458146874   R(theta=pi/2, phi=0) [km] 
10.7055386885955   R(theta=pi/2, phi=pi/2) [km] 
11.26754779243953   R(theta=pi/2, phi=pi) [km] 
1.58886566439251   Baryon mass of star 2  [M_sol] 
0.003041722311099634   relative variation enth. star 2
10.71157954289784   R(theta=0) [km] 
11.39143458146874   R(theta=pi/2, phi=0) [km] 
10.7055386885955   R(theta=pi/2, phi=pi/2) [km] 
11.26754779243953   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
11    step
0    abscidia of the rotation axis [km] 
2080.6049162998    Orbital frequency Omega [rad/s] 
0.03491009796841471    Abscidia ``center of mass'' star 1 [km] 
0.03491009796841471    Abscidia ``center of mass'' star 2 [km] 
1.602459316339989   Baryon mass of star 1  [M_sol] 
0.004463963874973088   relative variation enth. star 1
10.77307443667045   R(theta=0) [km] 
11.43698499049478   R(theta=pi/2, phi=0) [km] 
10.72896150186736   R(theta=pi/2, phi=pi/2) [km] 
11.31540799801113   R(theta=pi/2, phi=pi) [km] 
1.602459316339989   Baryon mass of star 2  [M_sol] 
0.004463963874973088   relative variation enth. star 2
10.77307443667045   R(theta=0) [km] 
11.43698499049478   R(theta=pi/2, phi=0) [km] 
10.72896150186736   R(theta=pi/2, phi=pi/2) [km] 
11.31540799801113   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
12    step
0    abscidia of the rotation axis [km] 
2129.391266276425    Orbital frequency Omega [rad/s] 
0.04325093003135549    Abscidia ``center of mass'' star 1 [km] 
0.04325093003135549    Abscidia ``center of mass'' star 2 [km] 
1.597799416925231   Baryon mass of star 1  [M_sol] 
0.002378872932359122   relative variation enth. star 1
10.77378666546616   R(theta=0) [km] 
11.4210986713137   R(theta=pi/2, phi=0) [km] 
10.69568119082676   R(theta=pi/2, phi=pi/2) [km] 
11.34522176641045   R(theta=pi/2, phi=pi) [km] 
1.597799416925231   Baryon mass of star 2  [M_sol] 
0.002378872932359122   relative variation enth. star 2
10.77378666546616   R(theta=0) [km] 
11.4210986713137   R(theta=pi/2, phi=0) [km] 
10.69568119082676   R(theta=pi/2, phi=pi/2) [km] 
11.34522176641045   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
13    step
0    abscidia of the rotation axis [km] 
2163.269222632182    Orbital frequency Omega [rad/s] 
0.03107592504565604    Abscidia ``center of mass'' star 1 [km] 
0.03107592504565604    Abscidia ``center of mass'' star 2 [km] 
1.587520691273237   Baryon mass of star 1  [M_sol] 
0.0009648803417801565   relative variation enth. star 1
10.75842614774053   R(theta=0) [km] 
11.36532781547115   R(theta=pi/2, phi=0) [km] 
10.66289468373076   R(theta=pi/2, phi=pi/2) [km] 
11.36585267548274   R(theta=pi/2, phi=pi) [km] 
1.587520691273237   Baryon mass of star 2  [M_sol] 
0.0009648803417801565   relative variation enth. star 2
10.75842614774053   R(theta=0) [km] 
11.36532781547115   R(theta=pi/2, phi=0) [km] 
10.66289468373076   R(theta=pi/2, phi=pi/2) [km] 
11.36585267548274   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
14    step
0    abscidia of the rotation axis [km] 
2159.450202792002    Orbital frequency Omega [rad/s] 
0.01054758283091539    Abscidia ``center of mass'' star 1 [km] 
0.01054758283091539    Abscidia ``center of mass'' star 2 [km] 
1.587748066673463   Baryon mass of star 1  [M_sol] 
0.001063188158696877   relative variation enth. star 1
10.75729730575734   R(theta=0) [km] 
11.36768984544293   R(theta=pi/2, phi=0) [km] 
10.66509654470352   R(theta=pi/2, phi=pi/2) [km] 
11.389411185829   R(theta=pi/2, phi=pi) [km] 
1.587748066673463   Baryon mass of star 2  [M_sol] 
0.001063188158696877   relative variation enth. star 2
10.75729730575734   R(theta=0) [km] 
11.36768984544293   R(theta=pi/2, phi=0) [km] 
10.66509654470352   R(theta=pi/2, phi=pi/2) [km] 
11.389411185829   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
15    step
0    abscidia of the rotation axis [km] 
2131.643770949136    Orbital frequency Omega [rad/s] 
0.0002900197308841612    Abscidia ``center of mass'' star 1 [km] 
0.0002900197308841612    Abscidia ``center of mass'' star 2 [km] 
1.589556569372619   Baryon mass of star 1  [M_sol] 
0.001295372835957838   relative variation enth. star 1
10.75960595634881   R(theta=0) [km] 
11.39125478241102   R(theta=pi/2, phi=0) [km] 
10.67376434420846   R(theta=pi/2, phi=pi/2) [km] 
11.39659218185373   R(theta=pi/2, phi=pi) [km] 
1.589556569372619   Baryon mass of star 2  [M_sol] 
0.001295372835957838   relative variation enth. star 2
10.75960595634881   R(theta=0) [km] 
11.39125478241102   R(theta=pi/2, phi=0) [km] 
10.67376434420846   R(theta=pi/2, phi=pi/2) [km] 
11.39659218185373   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
16    step
0    abscidia of the rotation axis [km] 
2146.978640365017    Orbital frequency Omega [rad/s] 
-0.003428294649263641    Abscidia ``center of mass'' star 1 [km] 
-0.003428294649263641    Abscidia ``center of mass'' star 2 [km] 
1.580094676749892   Baryon mass of star 1  [M_sol] 
0.001165765349333302   relative variation enth. star 1
10.75706466406367   R(theta=0) [km] 
11.44013641910502   R(theta=pi/2, phi=0) [km] 
10.66653831646302   R(theta=pi/2, phi=pi/2) [km] 
11.32055584082006   R(theta=pi/2, phi=pi) [km] 
1.580094676749892   Baryon mass of star 2  [M_sol] 
0.001165765349333302   relative variation enth. star 2
10.75706466406367   R(theta=0) [km] 
11.44013641910502   R(theta=pi/2, phi=0) [km] 
10.66653831646302   R(theta=pi/2, phi=pi/2) [km] 
11.32055584082006   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
17    step
0    abscidia of the rotation axis [km] 
2140.981300078086    Orbital frequency Omega [rad/s] 
0.02846714489739055    Abscidia ``center of mass'' star 1 [km] 
0.02846714489739055    Abscidia ``center of mass'' star 2 [km] 
1.593708185533917   Baryon mass of star 1  [M_sol] 
0.000449966021422591   relative variation enth. star 1
10.78791746461422   R(theta=0) [km] 
11.45264184391229   R(theta=pi/2, phi=0) [km] 
10.69951953640341   R(theta=pi/2, phi=pi/2) [km] 
11.37862902413353   R(theta=pi/2, phi=pi) [km] 
1.593708185533917   Baryon mass of star 2  [M_sol] 
0.000449966021422591   relative variation enth. star 2
10.78791746461422   R(theta=0) [km] 
11.45264184391229   R(theta=pi/2, phi=0) [km] 
10.69951953640341   R(theta=pi/2, phi=pi/2) [km] 
11.37862902413353   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
18    step
0    abscidia of the rotation axis [km] 
2140.848371127269    Orbital frequency Omega [rad/s] 
0.01552629584920862    Abscidia ``center of mass'' star 1 [km] 
0.01552629584920862    Abscidia ``center of mass'' star 2 [km] 
1.601464917659889   Baryon mass of star 1  [M_sol] 
0.000774692021989758   relative variation enth. star 1
10.80691845714998   R(theta=0) [km] 
11.46598952809017   R(theta=pi/2, phi=0) [km] 
10.71899457894388   R(theta=pi/2, phi=pi/2) [km] 
11.41105784643026   R(theta=pi/2, phi=pi) [km] 
1.601464917659889   Baryon mass of star 2  [M_sol] 
0.000774692021989758   relative variation enth. star 2
10.80691845714998   R(theta=0) [km] 
11.46598952809017   R(theta=pi/2, phi=0) [km] 
10.71899457894388   R(theta=pi/2, phi=pi/2) [km] 
11.41105784643026   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
19    step
0    abscidia of the rotation axis [km] 
2140.791764236632    Orbital frequency Omega [rad/s] 
0.01103297660161928    Abscidia ``center of mass'' star 1 [km] 
0.01103297660161928    Abscidia ``center of mass'' star 2 [km] 
1.60364017106457   Baryon mass of star 1  [M_sol] 
0.0007555155309226629   relative variation enth. star 1
10.8138841315653   R(theta=0) [km] 
11.47187059743657   R(theta=pi/2, phi=0) [km] 
10.72553673897052   R(theta=pi/2, phi=pi/2) [km] 
11.4238485422698   R(theta=pi/2, phi=pi) [km] 
1.60364017106457   Baryon mass of star 2  [M_sol] 
0.0007555155309226629   relative variation enth. star 2
10.8138841315653   R(theta=0) [km] 
11.47187059743657   R(theta=pi/2, phi=0) [km] 
10.72553673897052   R(theta=pi/2, phi=pi/2) [km] 
11.4238485422698   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
20    step
0    abscidia of the rotation axis [km] 
2141.463607955741    Orbital frequency Omega [rad/s] 
0.01089900226288965    Abscidia ``center of mass'' star 1 [km] 
0.01089900226288965    Abscidia ``center of mass'' star 2 [km] 
1.609227311611432   Baryon mass of star 1  [M_sol] 
0.0001192214662027238   relative variation enth. star 1
10.80602357888232   R(theta=0) [km] 
11.48380683017427   R(theta=pi/2, phi=0) [km] 
10.71674780353202   R(theta=pi/2, phi=pi/2) [km] 
11.41483251146472   R(theta=pi/2, phi=pi) [km] 
1.609227311611432   Baryon mass of star 2  [M_sol] 
0.0001192214662027238   relative variation enth. star 2
10.80602357888232   R(theta=0) [km] 
11.48380683017427   R(theta=pi/2, phi=0) [km] 
10.71674780353202   R(theta=pi/2, phi=pi/2) [km] 
11.41483251146472   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
21    step
0    abscidia of the rotation axis [km] 
2149.006363226732    Orbital frequency Omega [rad/s] 
0.01515891208667908    Abscidia ``center of mass'' star 1 [km] 
0.01515891208667908    Abscidia ``center of mass'' star 2 [km] 
1.603726318754272   Baryon mass of star 1  [M_sol] 
0.0005553544896466631   relative variation enth. star 1
10.79444690932221   R(theta=0) [km] 
11.47830977710531   R(theta=pi/2, phi=0) [km] 
10.70376725341994   R(theta=pi/2, phi=pi/2) [km] 
11.40470464660942   R(theta=pi/2, phi=pi) [km] 
1.603726318754272   Baryon mass of star 2  [M_sol] 
0.0005553544896466631   relative variation enth. star 2
10.79444690932221   R(theta=0) [km] 
11.47830977710531   R(theta=pi/2, phi=0) [km] 
10.70376725341994   R(theta=pi/2, phi=pi/2) [km] 
11.40470464660942   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
22    step
0    abscidia of the rotation axis [km] 
2158.433825382627    Orbital frequency Omega [rad/s] 
0.0173909019451024    Abscidia ``center of mass'' star 1 [km] 
0.0173909019451024    Abscidia ``center of mass'' star 2 [km] 
1.601624341228549   Baryon mass of star 1  [M_sol] 
0.0003791357618860063   relative variation enth. star 1
10.79072553192255   R(theta=0) [km] 
11.4660178106337   R(theta=pi/2, phi=0) [km] 
10.69861590391569   R(theta=pi/2, phi=pi/2) [km] 
11.40875371139041   R(theta=pi/2, phi=pi) [km] 
1.601624341228549   Baryon mass of star 2  [M_sol] 
0.0003791357618860063   relative variation enth. star 2
10.79072553192255   R(theta=0) [km] 
11.4660178106337   R(theta=pi/2, phi=0) [km] 
10.69861590391569   R(theta=pi/2, phi=pi/2) [km] 
11.40875371139041   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
23    step
0    abscidia of the rotation axis [km] 
2162.095562023933    Orbital frequency Omega [rad/s] 
0.01406195186651304    Abscidia ``center of mass'' star 1 [km] 
0.01406195186651304    Abscidia ``center of mass'' star 2 [km] 
1.601088917007008   Baryon mass of star 1  [M_sol] 
0.0002258178662808348   relative variation enth. star 1
10.78973883517554   R(theta=0) [km] 
11.46348969801169   R(theta=pi/2, phi=0) [km] 
10.69705763038682   R(theta=pi/2, phi=pi/2) [km] 
11.41417259814859   R(theta=pi/2, phi=pi) [km] 
1.601088917007008   Baryon mass of star 2  [M_sol] 
0.0002258178662808348   relative variation enth. star 2
10.78973883517554   R(theta=0) [km] 
11.46348969801169   R(theta=pi/2, phi=0) [km] 
10.69705763038682   R(theta=pi/2, phi=pi/2) [km] 
11.41417259814859   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
24    step
0    abscidia of the rotation axis [km] 
2159.807521699915    Orbital frequency Omega [rad/s] 
0.01182761157856538    Abscidia ``center of mass'' star 1 [km] 
0.01182761157856538    Abscidia ``center of mass'' star 2 [km] 
1.600358923134588   Baryon mass of star 1  [M_sol] 
0.0002346286045972569   relative variation enth. star 1
10.78963337362546   R(theta=0) [km] 
11.46673466828604   R(theta=pi/2, phi=0) [km] 
10.69822122198359   R(theta=pi/2, phi=pi/2) [km] 
11.41886339596225   R(theta=pi/2, phi=pi) [km] 
1.600358923134588   Baryon mass of star 2  [M_sol] 
0.0002346286045972569   relative variation enth. star 2
10.78963337362546   R(theta=0) [km] 
11.46673466828604   R(theta=pi/2, phi=0) [km] 
10.69822122198359   R(theta=pi/2, phi=pi/2) [km] 
11.41886339596225   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
25    step
0    abscidia of the rotation axis [km] 
2156.280902219818    Orbital frequency Omega [rad/s] 
0.01003415132996066    Abscidia ``center of mass'' star 1 [km] 
0.01003415132996066    Abscidia ``center of mass'' star 2 [km] 
1.601324416551478   Baryon mass of star 1  [M_sol] 
0.0001878565828372295   relative variation enth. star 1
10.79124570273376   R(theta=0) [km] 
11.47286695775943   R(theta=pi/2, phi=0) [km] 
10.70092274305948   R(theta=pi/2, phi=pi/2) [km] 
11.42180751037982   R(theta=pi/2, phi=pi) [km] 
1.601324416551478   Baryon mass of star 2  [M_sol] 
0.0001878565828372295   relative variation enth. star 2
10.79124570273376   R(theta=0) [km] 
11.47286695775943   R(theta=pi/2, phi=0) [km] 
10.70092274305948   R(theta=pi/2, phi=pi/2) [km] 
11.42180751037982   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
26    step
0    abscidia of the rotation axis [km] 
2152.385291022166    Orbital frequency Omega [rad/s] 
0.009949201044368028    Abscidia ``center of mass'' star 1 [km] 
0.009949201044368028    Abscidia ``center of mass'' star 2 [km] 
1.601801345641465   Baryon mass of star 1  [M_sol] 
0.0001585541510028469   relative variation enth. star 1
10.792342125415   R(theta=0) [km] 
11.4775419303854   R(theta=pi/2, phi=0) [km] 
10.70267230006899   R(theta=pi/2, phi=pi/2) [km] 
11.42066508901493   R(theta=pi/2, phi=pi) [km] 
1.601801345641465   Baryon mass of star 2  [M_sol] 
0.0001585541510028469   relative variation enth. star 2
10.792342125415   R(theta=0) [km] 
11.4775419303854   R(theta=pi/2, phi=0) [km] 
10.70267230006899   R(theta=pi/2, phi=pi/2) [km] 
11.42066508901493   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
27    step
0    abscidia of the rotation axis [km] 
2150.9553609769    Orbital frequency Omega [rad/s] 
0.01113625589219236    Abscidia ``center of mass'' star 1 [km] 
0.01113625589219236    Abscidia ``center of mass'' star 2 [km] 
1.600796345426107   Baryon mass of star 1  [M_sol] 
0.000577021555835143   relative variation enth. star 1
10.79101793960023   R(theta=0) [km] 
11.47851164392768   R(theta=pi/2, phi=0) [km] 
10.70137297324541   R(theta=pi/2, phi=pi/2) [km] 
11.4178328675432   R(theta=pi/2, phi=pi) [km] 
1.600796345426107   Baryon mass of star 2  [M_sol] 
0.000577021555835143   relative variation enth. star 2
10.79101793960023   R(theta=0) [km] 
11.47851164392768   R(theta=pi/2, phi=0) [km] 
10.70137297324541   R(theta=pi/2, phi=pi/2) [km] 
11.4178328675432   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
28    step
0    abscidia of the rotation axis [km] 
2147.234475459609    Orbital frequency Omega [rad/s] 
0.0117925899473259    Abscidia ``center of mass'' star 1 [km] 
0.0117925899473259    Abscidia ``center of mass'' star 2 [km] 
1.597824966982831   Baryon mass of star 1  [M_sol] 
0.001139767179373438   relative variation enth. star 1
10.78614279190764   R(theta=0) [km] 
11.47156779492732   R(theta=pi/2, phi=0) [km] 
10.69731706076627   R(theta=pi/2, phi=pi/2) [km] 
11.41873044722835   R(theta=pi/2, phi=pi) [km] 
1.597824966982831   Baryon mass of star 2  [M_sol] 
0.001139767179373438   relative variation enth. star 2
10.78614279190764   R(theta=0) [km] 
11.47156779492732   R(theta=pi/2, phi=0) [km] 
10.69731706076627   R(theta=pi/2, phi=pi/2) [km] 
11.41873044722835   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
29    step
0    abscidia of the rotation axis [km] 
2147.298561896952    Orbital frequency Omega [rad/s] 
0.009303244894662832    Abscidia ``center of mass'' star 1 [km] 
0.009303244894662832    Abscidia ``center of mass'' star 2 [km] 
1.594369041591357   Baryon mass of star 1  [M_sol] 
0.001857354230702583   relative variation enth. star 1
10.780870990438   R(theta=0) [km] 
11.47249830251852   R(theta=pi/2, phi=0) [km] 
10.69171623842401   R(theta=pi/2, phi=pi/2) [km] 
11.4079797649306   R(theta=pi/2, phi=pi) [km] 
1.594369041591357   Baryon mass of star 2  [M_sol] 
0.001857354230702583   relative variation enth. star 2
10.780870990438   R(theta=0) [km] 
11.47249830251852   R(theta=pi/2, phi=0) [km] 
10.69171623842401   R(theta=pi/2, phi=pi/2) [km] 
11.4079797649306   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
30    step
0    abscidia of the rotation axis [km] 
2149.122970434815    Orbital frequency Omega [rad/s] 
0.01257314386736041    Abscidia ``center of mass'' star 1 [km] 
0.01257314386736041    Abscidia ``center of mass'' star 2 [km] 
1.593635055907749   Baryon mass of star 1  [M_sol] 
0.002739956876189536   relative variation enth. star 1
10.78339067192941   R(theta=0) [km] 
11.4768517039551   R(theta=pi/2, phi=0) [km] 
10.69329840375121   R(theta=pi/2, phi=pi/2) [km] 
11.41020958831293   R(theta=pi/2, phi=pi) [km] 
1.593635055907749   Baryon mass of star 2  [M_sol] 
0.002739956876189536   relative variation enth. star 2
10.78339067192941   R(theta=0) [km] 
11.4768517039551   R(theta=pi/2, phi=0) [km] 
10.69329840375121   R(theta=pi/2, phi=pi/2) [km] 
11.41020958831293   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
31    step
0    abscidia of the rotation axis [km] 
2151.284538568252    Orbital frequency Omega [rad/s] 
0.01333874871785046    Abscidia ``center of mass'' star 1 [km] 
0.01333874871785046    Abscidia ``center of mass'' star 2 [km] 
1.590529415619668   Baryon mass of star 1  [M_sol] 
0.003491375095182325   relative variation enth. star 1
10.781694260453   R(theta=0) [km] 
11.47505674609727   R(theta=pi/2, phi=0) [km] 
10.69050052670469   R(theta=pi/2, phi=pi/2) [km] 
11.40953398812264   R(theta=pi/2, phi=pi) [km] 
1.590529415619668   Baryon mass of star 2  [M_sol] 
0.003491375095182325   relative variation enth. star 2
10.781694260453   R(theta=0) [km] 
11.47505674609727   R(theta=pi/2, phi=0) [km] 
10.69050052670469   R(theta=pi/2, phi=pi/2) [km] 
11.40953398812264   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
32    step
0    abscidia of the rotation axis [km] 
2150.899384932308    Orbital frequency Omega [rad/s] 
0.01285434015285247    Abscidia ``center of mass'' star 1 [km] 
0.01285434015285247    Abscidia ``center of mass'' star 2 [km] 
1.576750813119892   Baryon mass of star 1  [M_sol] 
0.003987756090579429   relative variation enth. star 1
10.77731053306579   R(theta=0) [km] 
11.46266661991016   R(theta=pi/2, phi=0) [km] 
10.68746587672404   R(theta=pi/2, phi=pi/2) [km] 
11.41098562674578   R(theta=pi/2, phi=pi) [km] 
1.576750813119892   Baryon mass of star 2  [M_sol] 
0.003987756090579429   relative variation enth. star 2
10.77731053306579   R(theta=0) [km] 
11.46266661991016   R(theta=pi/2, phi=0) [km] 
10.68746587672404   R(theta=pi/2, phi=pi/2) [km] 
11.41098562674578   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
33    step
0    abscidia of the rotation axis [km] 
2148.062082173914    Orbital frequency Omega [rad/s] 
0.00906388270823788    Abscidia ``center of mass'' star 1 [km] 
0.00906388270823788    Abscidia ``center of mass'' star 2 [km] 
1.580735061150766   Baryon mass of star 1  [M_sol] 
0.004060117911664501   relative variation enth. star 1
10.79151404455976   R(theta=0) [km] 
11.48234207145826   R(theta=pi/2, phi=0) [km] 
10.70195505372179   R(theta=pi/2, phi=pi/2) [km] 
11.42502421602738   R(theta=pi/2, phi=pi) [km] 
1.580735061150766   Baryon mass of star 2  [M_sol] 
0.004060117911664501   relative variation enth. star 2
10.79151404455976   R(theta=0) [km] 
11.48234207145826   R(theta=pi/2, phi=0) [km] 
10.70195505372179   R(theta=pi/2, phi=pi/2) [km] 
11.42502421602738   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
34    step
0    abscidia of the rotation axis [km] 
2144.485680967015    Orbital frequency Omega [rad/s] 
0.009879372214769422    Abscidia ``center of mass'' star 1 [km] 
0.009879372214769422    Abscidia ``center of mass'' star 2 [km] 
1.580847172956214   Baryon mass of star 1  [M_sol] 
0.004592748888164513   relative variation enth. star 1
10.79817598882756   R(theta=0) [km] 
11.49426518759549   R(theta=pi/2, phi=0) [km] 
10.7081737868655   R(theta=pi/2, phi=pi/2) [km] 
11.42853757056418   R(theta=pi/2, phi=pi) [km] 
1.580847172956214   Baryon mass of star 2  [M_sol] 
0.004592748888164513   relative variation enth. star 2
10.79817598882756   R(theta=0) [km] 
11.49426518759549   R(theta=pi/2, phi=0) [km] 
10.7081737868655   R(theta=pi/2, phi=pi/2) [km] 
11.42853757056418   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
35    step
0    abscidia of the rotation axis [km] 
2143.522650413842    Orbital frequency Omega [rad/s] 
0.01179697069221497    Abscidia ``center of mass'' star 1 [km] 
0.01179697069221497    Abscidia ``center of mass'' star 2 [km] 
1.577147085073165   Baryon mass of star 1  [M_sol] 
0.005336573307908848   relative variation enth. star 1
10.79720181289393   R(theta=0) [km] 
11.49654090176512   R(theta=pi/2, phi=0) [km] 
10.70657919397721   R(theta=pi/2, phi=pi/2) [km] 
11.42690956455356   R(theta=pi/2, phi=pi) [km] 
1.577147085073165   Baryon mass of star 2  [M_sol] 
0.005336573307908848   relative variation enth. star 2
10.79720181289393   R(theta=0) [km] 
11.49654090176512   R(theta=pi/2, phi=0) [km] 
10.70657919397721   R(theta=pi/2, phi=pi/2) [km] 
11.42690956455356   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
36    step
0    abscidia of the rotation axis [km] 
2140.361198499966    Orbital frequency Omega [rad/s] 
0.01219982978002632    Abscidia ``center of mass'' star 1 [km] 
0.01219982978002632    Abscidia ``center of mass'' star 2 [km] 
1.559144070382266   Baryon mass of star 1  [M_sol] 
0.005584704038566032   relative variation enth. star 1
10.79215116850608   R(theta=0) [km] 
11.48162080237546   R(theta=pi/2, phi=0) [km] 
10.7039286807351   R(theta=pi/2, phi=pi/2) [km] 
11.43206449301222   R(theta=pi/2, phi=pi) [km] 
1.559144070382266   Baryon mass of star 2  [M_sol] 
0.005584704038566032   relative variation enth. star 2
10.79215116850608   R(theta=0) [km] 
11.48162080237546   R(theta=pi/2, phi=0) [km] 
10.7039286807351   R(theta=pi/2, phi=pi/2) [km] 
11.43206449301222   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
37    step
0    abscidia of the rotation axis [km] 
2137.831129167032    Orbital frequency Omega [rad/s] 
0.006882062970601233    Abscidia ``center of mass'' star 1 [km] 
0.006882062970601233    Abscidia ``center of mass'' star 2 [km] 
1.566379995541022   Baryon mass of star 1  [M_sol] 
0.004770065658681941   relative variation enth. star 1
10.81491110610131   R(theta=0) [km] 
11.51460047731241   R(theta=pi/2, phi=0) [km] 
10.72661136214293   R(theta=pi/2, phi=pi/2) [km] 
11.45009734201489   R(theta=pi/2, phi=pi) [km] 
1.566379995541022   Baryon mass of star 2  [M_sol] 
0.004770065658681941   relative variation enth. star 2
10.81491110610131   R(theta=0) [km] 
11.51460047731241   R(theta=pi/2, phi=0) [km] 
10.72661136214293   R(theta=pi/2, phi=pi/2) [km] 
11.45009734201489   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
38    step
0    abscidia of the rotation axis [km] 
2136.51833774297    Orbital frequency Omega [rad/s] 
0.01062468703326491    Abscidia ``center of mass'' star 1 [km] 
0.01062468703326491    Abscidia ``center of mass'' star 2 [km] 
1.567356180420272   Baryon mass of star 1  [M_sol] 
0.005307561969599942   relative variation enth. star 1
10.82536155265141   R(theta=0) [km] 
11.52947879379911   R(theta=pi/2, phi=0) [km] 
10.73514736508838   R(theta=pi/2, phi=pi/2) [km] 
11.45713631242151   R(theta=pi/2, phi=pi) [km] 
1.567356180420272   Baryon mass of star 2  [M_sol] 
0.005307561969599942   relative variation enth. star 2
10.82536155265141   R(theta=0) [km] 
11.52947879379911   R(theta=pi/2, phi=0) [km] 
10.73514736508838   R(theta=pi/2, phi=pi/2) [km] 
11.45713631242151   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
39    step
0    abscidia of the rotation axis [km] 
2137.735874184902    Orbital frequency Omega [rad/s] 
0.01271945874762981    Abscidia ``center of mass'' star 1 [km] 
0.01271945874762981    Abscidia ``center of mass'' star 2 [km] 
1.563421905211739   Baryon mass of star 1  [M_sol] 
0.005986515696768424   relative variation enth. star 1
10.82515283559497   R(theta=0) [km] 
11.53087274266465   R(theta=pi/2, phi=0) [km] 
10.73345701626002   R(theta=pi/2, phi=pi/2) [km] 
11.45756563357105   R(theta=pi/2, phi=pi) [km] 
1.563421905211739   Baryon mass of star 2  [M_sol] 
0.005986515696768424   relative variation enth. star 2
10.82515283559497   R(theta=0) [km] 
11.53087274266465   R(theta=pi/2, phi=0) [km] 
10.73345701626002   R(theta=pi/2, phi=pi/2) [km] 
11.45756563357105   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
40    step
0    abscidia of the rotation axis [km] 
2135.756917722823    Orbital frequency Omega [rad/s] 
0.01275669194157114    Abscidia ``center of mass'' star 1 [km] 
0.01275669194157114    Abscidia ``center of mass'' star 2 [km] 
1.54477277631197   Baryon mass of star 1  [M_sol] 
0.005907200595123169   relative variation enth. star 1
10.8206213622394   R(theta=0) [km] 
11.51904360348403   R(theta=pi/2, phi=0) [km] 
10.73104625304352   R(theta=pi/2, phi=pi/2) [km] 
11.46314683729076   R(theta=pi/2, phi=pi) [km] 
1.54477277631197   Baryon mass of star 2  [M_sol] 
0.005907200595123169   relative variation enth. star 2
10.8206213622394   R(theta=0) [km] 
11.51904360348403   R(theta=pi/2, phi=0) [km] 
10.73104625304352   R(theta=pi/2, phi=pi/2) [km] 
11.46314683729076   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
41    step
0    abscidia of the rotation axis [km] 
2133.319356785252    Orbital frequency Omega [rad/s] 
0.008016891458264563    Abscidia ``center of mass'' star 1 [km] 
0.008016891458264563    Abscidia ``center of mass'' star 2 [km] 
1.554540503386474   Baryon mass of star 1  [M_sol] 
0.004396252258732486   relative variation enth. star 1
10.84878145453776   R(theta=0) [km] 
11.55594623968461   R(theta=pi/2, phi=0) [km] 
10.75926588996336   R(theta=pi/2, phi=pi/2) [km] 
11.48978944030753   R(theta=pi/2, phi=pi) [km] 
1.554540503386474   Baryon mass of star 2  [M_sol] 
0.004396252258732486   relative variation enth. star 2
10.84878145453776   R(theta=0) [km] 
11.55594623968461   R(theta=pi/2, phi=0) [km] 
10.75926588996336   R(theta=pi/2, phi=pi/2) [km] 
11.48978944030753   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
42    step
0    abscidia of the rotation axis [km] 
2130.778617336402    Orbital frequency Omega [rad/s] 
0.01031375271762336    Abscidia ``center of mass'' star 1 [km] 
0.01031375271762336    Abscidia ``center of mass'' star 2 [km] 
1.554789070037635   Baryon mass of star 1  [M_sol] 
0.004879929478011686   relative variation enth. star 1
10.85688728585054   R(theta=0) [km] 
11.56777046910606   R(theta=pi/2, phi=0) [km] 
10.76568392807593   R(theta=pi/2, phi=pi/2) [km] 
11.49487457334748   R(theta=pi/2, phi=pi) [km] 
1.554789070037635   Baryon mass of star 2  [M_sol] 
0.004879929478011686   relative variation enth. star 2
10.85688728585054   R(theta=0) [km] 
11.56777046910606   R(theta=pi/2, phi=0) [km] 
10.76568392807593   R(theta=pi/2, phi=pi/2) [km] 
11.49487457334748   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
43    step
0    abscidia of the rotation axis [km] 
2130.378816933251    Orbital frequency Omega [rad/s] 
0.0119048612254935    Abscidia ``center of mass'' star 1 [km] 
0.0119048612254935    Abscidia ``center of mass'' star 2 [km] 
1.551375495363146   Baryon mass of star 1  [M_sol] 
0.005392362791866737   relative variation enth. star 1
10.85660164351731   R(theta=0) [km] 
11.57007939437811   R(theta=pi/2, phi=0) [km] 
10.76451389512764   R(theta=pi/2, phi=pi/2) [km] 
11.49481931480965   R(theta=pi/2, phi=pi) [km] 
1.551375495363146   Baryon mass of star 2  [M_sol] 
0.005392362791866737   relative variation enth. star 2
10.85660164351731   R(theta=0) [km] 
11.57007939437811   R(theta=pi/2, phi=0) [km] 
10.76451389512764   R(theta=pi/2, phi=pi/2) [km] 
11.49481931480965   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
44    step
0    abscidia of the rotation axis [km] 
2128.029381432595    Orbital frequency Omega [rad/s] 
0.01213851429120449    Abscidia ``center of mass'' star 1 [km] 
0.01213851429120449    Abscidia ``center of mass'' star 2 [km] 
1.535905878342688   Baryon mass of star 1  [M_sol] 
0.00524363005810896   relative variation enth. star 1
10.85423221684976   R(theta=0) [km] 
11.56477509596334   R(theta=pi/2, phi=0) [km] 
10.7640438980831   R(theta=pi/2, phi=pi/2) [km] 
11.50067800243927   R(theta=pi/2, phi=pi) [km] 
1.535905878342688   Baryon mass of star 2  [M_sol] 
0.00524363005810896   relative variation enth. star 2
10.85423221684976   R(theta=0) [km] 
11.56477509596334   R(theta=pi/2, phi=0) [km] 
10.7640438980831   R(theta=pi/2, phi=pi/2) [km] 
11.50067800243927   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
45    step
0    abscidia of the rotation axis [km] 
2126.102825802323    Orbital frequency Omega [rad/s] 
0.008851250167687397    Abscidia ``center of mass'' star 1 [km] 
0.008851250167687397    Abscidia ``center of mass'' star 2 [km] 
1.545580042967654   Baryon mass of star 1  [M_sol] 
0.0034440969539835   relative variation enth. star 1
10.88117711136784   R(theta=0) [km] 
11.59936299062488   R(theta=pi/2, phi=0) [km] 
10.79095136410723   R(theta=pi/2, phi=pi/2) [km] 
11.52601576925824   R(theta=pi/2, phi=pi) [km] 
1.545580042967654   Baryon mass of star 2  [M_sol] 
0.0034440969539835   relative variation enth. star 2
10.88117711136784   R(theta=0) [km] 
11.59936299062488   R(theta=pi/2, phi=0) [km] 
10.79095136410723   R(theta=pi/2, phi=pi/2) [km] 
11.52601576925824   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
46    step
0    abscidia of the rotation axis [km] 
2124.75267463905    Orbital frequency Omega [rad/s] 
0.01110231128509387    Abscidia ``center of mass'' star 1 [km] 
0.01110231128509387    Abscidia ``center of mass'' star 2 [km] 
1.545084409056885   Baryon mass of star 1  [M_sol] 
0.004015265933070998   relative variation enth. star 1
10.88647841958436   R(theta=0) [km] 
11.60646907529703   R(theta=pi/2, phi=0) [km] 
10.79441569922911   R(theta=pi/2, phi=pi/2) [km] 
11.52927579920758   R(theta=pi/2, phi=pi) [km] 
1.545084409056885   Baryon mass of star 2  [M_sol] 
0.004015265933070998   relative variation enth. star 2
10.88647841958436   R(theta=0) [km] 
11.60646907529703   R(theta=pi/2, phi=0) [km] 
10.79441569922911   R(theta=pi/2, phi=pi/2) [km] 
11.52927579920758   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
47    step
0    abscidia of the rotation axis [km] 
2124.876066708341    Orbital frequency Omega [rad/s] 
0.01207316650859713    Abscidia ``center of mass'' star 1 [km] 
0.01207316650859713    Abscidia ``center of mass'' star 2 [km] 
1.542516299069246   Baryon mass of star 1  [M_sol] 
0.004366171858192379   relative variation enth. star 1
10.88665527789811   R(theta=0) [km] 
11.60830814829814   R(theta=pi/2, phi=0) [km] 
10.79380515614952   R(theta=pi/2, phi=pi/2) [km] 
11.53015310031618   R(theta=pi/2, phi=pi) [km] 
1.542516299069246   Baryon mass of star 2  [M_sol] 
0.004366171858192379   relative variation enth. star 2
10.88665527789811   R(theta=0) [km] 
11.60830814829814   R(theta=pi/2, phi=0) [km] 
10.79380515614952   R(theta=pi/2, phi=pi/2) [km] 
11.53015310031618   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
48    step
0    abscidia of the rotation axis [km] 
2123.70188313596    Orbital frequency Omega [rad/s] 
0.01217516343657632    Abscidia ``center of mass'' star 1 [km] 
0.01217516343657632    Abscidia ``center of mass'' star 2 [km] 
1.530950662521004   Baryon mass of star 1  [M_sol] 
0.004166799640471119   relative variation enth. star 1
10.88536814703246   R(theta=0) [km] 
11.6076557303231   R(theta=pi/2, phi=0) [km] 
10.79373504072541   R(theta=pi/2, phi=pi/2) [km] 
11.53446360912534   R(theta=pi/2, phi=pi) [km] 
1.530950662521004   Baryon mass of star 2  [M_sol] 
0.004166799640471119   relative variation enth. star 2
10.88536814703246   R(theta=0) [km] 
11.6076557303231   R(theta=pi/2, phi=0) [km] 
10.79373504072541   R(theta=pi/2, phi=pi/2) [km] 
11.53446360912534   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
49    step
0    abscidia of the rotation axis [km] 
2122.599390032544    Orbital frequency Omega [rad/s] 
0.01041763387730876    Abscidia ``center of mass'' star 1 [km] 
0.01041763387730876    Abscidia ``center of mass'' star 2 [km] 
1.539127163911004   Baryon mass of star 1  [M_sol] 
0.002462162479557015   relative variation enth. star 1
10.90763417920203   R(theta=0) [km] 
11.63480074212912   R(theta=pi/2, phi=0) [km] 
10.81595074781629   R(theta=pi/2, phi=pi/2) [km] 
11.55691017735648   R(theta=pi/2, phi=pi) [km] 
1.539127163911004   Baryon mass of star 2  [M_sol] 
0.002462162479557015   relative variation enth. star 2
10.90763417920203   R(theta=0) [km] 
11.63480074212912   R(theta=pi/2, phi=0) [km] 
10.81595074781629   R(theta=pi/2, phi=pi/2) [km] 
11.55691017735648   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
50    step
0    abscidia of the rotation axis [km] 
2121.583849995603    Orbital frequency Omega [rad/s] 
0.01151283039411366    Abscidia ``center of mass'' star 1 [km] 
0.01151283039411366    Abscidia ``center of mass'' star 2 [km] 
1.53792086575045   Baryon mass of star 1  [M_sol] 
0.003016202318678941   relative variation enth. star 1
10.90961672380206   R(theta=0) [km] 
11.63724521732627   R(theta=pi/2, phi=0) [km] 
10.81654310476526   R(theta=pi/2, phi=pi/2) [km] 
11.55800491466835   R(theta=pi/2, phi=pi) [km] 
1.53792086575045   Baryon mass of star 2  [M_sol] 
0.003016202318678941   relative variation enth. star 2
10.90961672380206   R(theta=0) [km] 
11.63724521732627   R(theta=pi/2, phi=0) [km] 
10.81654310476526   R(theta=pi/2, phi=pi/2) [km] 
11.55800491466835   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
51    step
0    abscidia of the rotation axis [km] 
2121.215170046091    Orbital frequency Omega [rad/s] 
0.01179617962911994    Abscidia ``center of mass'' star 1 [km] 
0.01179617962911994    Abscidia ``center of mass'' star 2 [km] 
1.536278778209349   Baryon mass of star 1  [M_sol] 
0.003164920289662094   relative variation enth. star 1
10.91003099174783   R(theta=0) [km] 
11.63917887156266   R(theta=pi/2, phi=0) [km] 
10.81661512399286   R(theta=pi/2, phi=pi/2) [km] 
11.55913237302834   R(theta=pi/2, phi=pi) [km] 
1.536278778209349   Baryon mass of star 2  [M_sol] 
0.003164920289662094   relative variation enth. star 2
10.91003099174783   R(theta=0) [km] 
11.63917887156266   R(theta=pi/2, phi=0) [km] 
10.81661512399286   R(theta=pi/2, phi=pi/2) [km] 
11.55913237302834   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
52    step
0    abscidia of the rotation axis [km] 
2120.318920322471    Orbital frequency Omega [rad/s] 
0.01184712687439804    Abscidia ``center of mass'' star 1 [km] 
0.01184712687439804    Abscidia ``center of mass'' star 2 [km] 
1.5285493277049   Baryon mass of star 1  [M_sol] 
0.0029369127678183   relative variation enth. star 1
10.90992617727118   R(theta=0) [km] 
11.64169929819858   R(theta=pi/2, phi=0) [km] 
10.8173450780246   R(theta=pi/2, phi=pi/2) [km] 
11.56240818699119   R(theta=pi/2, phi=pi) [km] 
1.5285493277049   Baryon mass of star 2  [M_sol] 
0.0029369127678183   relative variation enth. star 2
10.90992617727118   R(theta=0) [km] 
11.64169929819858   R(theta=pi/2, phi=0) [km] 
10.8173450780246   R(theta=pi/2, phi=pi/2) [km] 
11.56240818699119   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
53    step
0    abscidia of the rotation axis [km] 
2119.602359473931    Orbital frequency Omega [rad/s] 
0.01115576535728247    Abscidia ``center of mass'' star 1 [km] 
0.01115576535728247    Abscidia ``center of mass'' star 2 [km] 
1.534732274996111   Baryon mass of star 1  [M_sol] 
0.00155230763747726   relative variation enth. star 1
10.92639893427273   R(theta=0) [km] 
11.66135079282363   R(theta=pi/2, phi=0) [km] 
10.83377802201853   R(theta=pi/2, phi=pi/2) [km] 
11.57922930357806   R(theta=pi/2, phi=pi) [km] 
1.534732274996111   Baryon mass of star 2  [M_sol] 
0.00155230763747726   relative variation enth. star 2
10.92639893427273   R(theta=0) [km] 
11.66135079282363   R(theta=pi/2, phi=0) [km] 
10.83377802201853   R(theta=pi/2, phi=pi/2) [km] 
11.57922930357806   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
54    step
0    abscidia of the rotation axis [km] 
2119.159437451424    Orbital frequency Omega [rad/s] 
0.01186260322653343    Abscidia ``center of mass'' star 1 [km] 
0.01186260322653343    Abscidia ``center of mass'' star 2 [km] 
1.533618795346586   Baryon mass of star 1  [M_sol] 
0.001951019822342252   relative variation enth. star 1
10.92695705505998   R(theta=0) [km] 
11.66153874490764   R(theta=pi/2, phi=0) [km] 
10.83326919642657   R(theta=pi/2, phi=pi/2) [km] 
11.57944685768903   R(theta=pi/2, phi=pi) [km] 
1.533618795346586   Baryon mass of star 2  [M_sol] 
0.001951019822342252   relative variation enth. star 2
10.92695705505998   R(theta=0) [km] 
11.66153874490764   R(theta=pi/2, phi=0) [km] 
10.83326919642657   R(theta=pi/2, phi=pi/2) [km] 
11.57944685768903   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
55    step
0    abscidia of the rotation axis [km] 
2118.997020574946    Orbital frequency Omega [rad/s] 
0.01185695038672385    Abscidia ``center of mass'' star 1 [km] 
0.01185695038672385    Abscidia ``center of mass'' star 2 [km] 
1.532831530383518   Baryon mass of star 1  [M_sol] 
0.001961191403391047   relative variation enth. star 1
10.92770748895822   R(theta=0) [km] 
11.66319933027599   R(theta=pi/2, phi=0) [km] 
10.83380905275794   R(theta=pi/2, phi=pi/2) [km] 
11.58089863461423   R(theta=pi/2, phi=pi) [km] 
1.532831530383518   Baryon mass of star 2  [M_sol] 
0.001961191403391047   relative variation enth. star 2
10.92770748895822   R(theta=0) [km] 
11.66319933027599   R(theta=pi/2, phi=0) [km] 
10.83380905275794   R(theta=pi/2, phi=pi/2) [km] 
11.58089863461423   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
56    step
0    abscidia of the rotation axis [km] 
2118.636240261045    Orbital frequency Omega [rad/s] 
0.01187891291033027    Abscidia ``center of mass'' star 1 [km] 
0.01187891291033027    Abscidia ``center of mass'' star 2 [km] 
1.528044724325333   Baryon mass of star 1  [M_sol] 
0.001777208416514499   relative variation enth. star 1
10.9267898002443   R(theta=0) [km] 
11.66550899025533   R(theta=pi/2, phi=0) [km] 
10.83329721890036   R(theta=pi/2, phi=pi/2) [km] 
11.58145375924058   R(theta=pi/2, phi=pi) [km] 
1.528044724325333   Baryon mass of star 2  [M_sol] 
0.001777208416514499   relative variation enth. star 2
10.9267898002443   R(theta=0) [km] 
11.66550899025533   R(theta=pi/2, phi=0) [km] 
10.83329721890036   R(theta=pi/2, phi=pi/2) [km] 
11.58145375924058   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
57    step
0    abscidia of the rotation axis [km] 
2118.492258550538    Orbital frequency Omega [rad/s] 
0.0119366146441191    Abscidia ``center of mass'' star 1 [km] 
0.0119366146441191    Abscidia ``center of mass'' star 2 [km] 
1.531765889504298   Baryon mass of star 1  [M_sol] 
0.0009773280687903803   relative variation enth. star 1
10.93671499910983   R(theta=0) [km] 
11.67728846403665   R(theta=pi/2, phi=0) [km] 
10.8431909815019   R(theta=pi/2, phi=pi/2) [km] 
11.59228658634334   R(theta=pi/2, phi=pi) [km] 
1.531765889504298   Baryon mass of star 2  [M_sol] 
0.0009773280687903803   relative variation enth. star 2
10.93671499910983   R(theta=0) [km] 
11.67728846403665   R(theta=pi/2, phi=0) [km] 
10.8431909815019   R(theta=pi/2, phi=pi/2) [km] 
11.59228658634334   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
58    step
0    abscidia of the rotation axis [km] 
2118.446742587274    Orbital frequency Omega [rad/s] 
0.01216557294350995    Abscidia ``center of mass'' star 1 [km] 
0.01216557294350995    Abscidia ``center of mass'' star 2 [km] 
1.531081593576554   Baryon mass of star 1  [M_sol] 
0.001193256463210395   relative variation enth. star 1
10.9370307879409   R(theta=0) [km] 
11.67686253515536   R(theta=pi/2, phi=0) [km] 
10.84283858225137   R(theta=pi/2, phi=pi/2) [km] 
11.59285282542625   R(theta=pi/2, phi=pi) [km] 
1.531081593576554   Baryon mass of star 2  [M_sol] 
0.001193256463210395   relative variation enth. star 2
10.9370307879409   R(theta=0) [km] 
11.67686253515536   R(theta=pi/2, phi=0) [km] 
10.84283858225137   R(theta=pi/2, phi=pi/2) [km] 
11.59285282542625   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
59    step
0    abscidia of the rotation axis [km] 
2118.268255227144    Orbital frequency Omega [rad/s] 
0.01193091970693061    Abscidia ``center of mass'' star 1 [km] 
0.01193091970693061    Abscidia ``center of mass'' star 2 [km] 
1.530606296847535   Baryon mass of star 1  [M_sol] 
0.001193000498423641   relative variation enth. star 1
10.93745311710414   R(theta=0) [km] 
11.67790836505938   R(theta=pi/2, phi=0) [km] 
10.84315518178684   R(theta=pi/2, phi=pi/2) [km] 
11.59396841257457   R(theta=pi/2, phi=pi) [km] 
1.530606296847535   Baryon mass of star 2  [M_sol] 
0.001193000498423641   relative variation enth. star 2
10.93745311710414   R(theta=0) [km] 
11.67790836505938   R(theta=pi/2, phi=0) [km] 
10.84315518178684   R(theta=pi/2, phi=pi/2) [km] 
11.59396841257457   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
60    step
0    abscidia of the rotation axis [km] 
2118.054849534671    Orbital frequency Omega [rad/s] 
0.01185332317966914    Abscidia ``center of mass'' star 1 [km] 
0.01185332317966914    Abscidia ``center of mass'' star 2 [km] 
1.527570608419368   Baryon mass of star 1  [M_sol] 
0.001061751919490646   relative variation enth. star 1
10.9367023517085   R(theta=0) [km] 
11.67971872017588   R(theta=pi/2, phi=0) [km] 
10.84267530783034   R(theta=pi/2, phi=pi/2) [km] 
11.5937971127586   R(theta=pi/2, phi=pi) [km] 
1.527570608419368   Baryon mass of star 2  [M_sol] 
0.001061751919490646   relative variation enth. star 2
10.9367023517085   R(theta=0) [km] 
11.67971872017588   R(theta=pi/2, phi=0) [km] 
10.84267530783034   R(theta=pi/2, phi=pi/2) [km] 
11.5937971127586   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
61    step
0    abscidia of the rotation axis [km] 
2117.886995419628    Orbital frequency Omega [rad/s] 
0.0120588983089509    Abscidia ``center of mass'' star 1 [km] 
0.0120588983089509    Abscidia ``center of mass'' star 2 [km] 
1.530059790017721   Baryon mass of star 1  [M_sol] 
0.0005752294819218323   relative variation enth. star 1
10.94322551960298   R(theta=0) [km] 
11.68746201495501   R(theta=pi/2, phi=0) [km] 
10.84922049043089   R(theta=pi/2, phi=pi/2) [km] 
11.60113731337162   R(theta=pi/2, phi=pi) [km] 
1.530059790017721   Baryon mass of star 2  [M_sol] 
0.0005752294819218323   relative variation enth. star 2
10.94322551960298   R(theta=0) [km] 
11.68746201495501   R(theta=pi/2, phi=0) [km] 
10.84922049043089   R(theta=pi/2, phi=pi/2) [km] 
11.60113731337162   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
62    step
0    abscidia of the rotation axis [km] 
2117.820481442187    Orbital frequency Omega [rad/s] 
0.01212645990849914    Abscidia ``center of mass'' star 1 [km] 
0.01212645990849914    Abscidia ``center of mass'' star 2 [km] 
1.529615534652572   Baryon mass of star 1  [M_sol] 
0.0007307006511978613   relative variation enth. star 1
10.94337038422903   R(theta=0) [km] 
11.68707768195695   R(theta=pi/2, phi=0) [km] 
10.84895100489458   R(theta=pi/2, phi=pi/2) [km] 
11.60148798723872   R(theta=pi/2, phi=pi) [km] 
1.529615534652572   Baryon mass of star 2  [M_sol] 
0.0007307006511978613   relative variation enth. star 2
10.94337038422903   R(theta=0) [km] 
11.68707768195695   R(theta=pi/2, phi=0) [km] 
10.84895100489458   R(theta=pi/2, phi=pi/2) [km] 
11.60148798723872   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
63    step
0    abscidia of the rotation axis [km] 
2117.695927264604    Orbital frequency Omega [rad/s] 
0.01195520328608701    Abscidia ``center of mass'' star 1 [km] 
0.01195520328608701    Abscidia ``center of mass'' star 2 [km] 
1.529346140073583   Baryon mass of star 1  [M_sol] 
0.0007363573582035983   relative variation enth. star 1
10.9436881468608   R(theta=0) [km] 
11.68782882494067   R(theta=pi/2, phi=0) [km] 
10.84920319032515   R(theta=pi/2, phi=pi/2) [km] 
11.60227135204277   R(theta=pi/2, phi=pi) [km] 
1.529346140073583   Baryon mass of star 2  [M_sol] 
0.0007363573582035983   relative variation enth. star 2
10.9436881468608   R(theta=0) [km] 
11.68782882494067   R(theta=pi/2, phi=0) [km] 
10.84920319032515   R(theta=pi/2, phi=pi/2) [km] 
11.60227135204277   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
64    step
0    abscidia of the rotation axis [km] 
2117.529565393658    Orbital frequency Omega [rad/s] 
0.01191464912220885    Abscidia ``center of mass'' star 1 [km] 
0.01191464912220885    Abscidia ``center of mass'' star 2 [km] 
1.527501104131896   Baryon mass of star 1  [M_sol] 
0.0006557585783632849   relative variation enth. star 1
10.94309327205425   R(theta=0) [km] 
11.68901696217095   R(theta=pi/2, phi=0) [km] 
10.84877294093667   R(theta=pi/2, phi=pi/2) [km] 
11.60209639889409   R(theta=pi/2, phi=pi) [km] 
1.527501104131896   Baryon mass of star 2  [M_sol] 
0.0006557585783632849   relative variation enth. star 2
10.94309327205425   R(theta=0) [km] 
11.68901696217095   R(theta=pi/2, phi=0) [km] 
10.84877294093667   R(theta=pi/2, phi=pi/2) [km] 
11.60209639889409   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
65    step
0    abscidia of the rotation axis [km] 
2117.487688505889    Orbital frequency Omega [rad/s] 
0.0120489334915419    Abscidia ``center of mass'' star 1 [km] 
0.0120489334915419    Abscidia ``center of mass'' star 2 [km] 
1.528951032384906   Baryon mass of star 1  [M_sol] 
0.0003673849143145516   relative variation enth. star 1
10.94692967775187   R(theta=0) [km] 
11.69372033217048   R(theta=pi/2, phi=0) [km] 
10.8526098845404   R(theta=pi/2, phi=pi/2) [km] 
11.60638181472458   R(theta=pi/2, phi=pi) [km] 
1.528951032384906   Baryon mass of star 2  [M_sol] 
0.0003673849143145516   relative variation enth. star 2
10.94692967775187   R(theta=0) [km] 
11.69372033217048   R(theta=pi/2, phi=0) [km] 
10.8526098845404   R(theta=pi/2, phi=pi/2) [km] 
11.60638181472458   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
66    step
0    abscidia of the rotation axis [km] 
2117.541594764531    Orbital frequency Omega [rad/s] 
0.01214551175585443    Abscidia ``center of mass'' star 1 [km] 
0.01214551175585443    Abscidia ``center of mass'' star 2 [km] 
1.528707996919989   Baryon mass of star 1  [M_sol] 
0.0004551641514471319   relative variation enth. star 1
10.94710705296001   R(theta=0) [km] 
11.69351290768959   R(theta=pi/2, phi=0) [km] 
10.85251077148837   R(theta=pi/2, phi=pi/2) [km] 
11.6067444594387   R(theta=pi/2, phi=pi) [km] 
1.528707996919989   Baryon mass of star 2  [M_sol] 
0.0004551641514471319   relative variation enth. star 2
10.94710705296001   R(theta=0) [km] 
11.69351290768959   R(theta=pi/2, phi=0) [km] 
10.85251077148837   R(theta=pi/2, phi=pi/2) [km] 
11.6067444594387   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
67    step
0    abscidia of the rotation axis [km] 
2117.516100610508    Orbital frequency Omega [rad/s] 
0.01202173520238548    Abscidia ``center of mass'' star 1 [km] 
0.01202173520238548    Abscidia ``center of mass'' star 2 [km] 
1.528520296769601   Baryon mass of star 1  [M_sol] 
0.0004577619846406276   relative variation enth. star 1
10.94726445953118   R(theta=0) [km] 
11.69389285127781   R(theta=pi/2, phi=0) [km] 
10.85260885026448   R(theta=pi/2, phi=pi/2) [km] 
11.60724265094991   R(theta=pi/2, phi=pi) [km] 
1.528520296769601   Baryon mass of star 2  [M_sol] 
0.0004577619846406276   relative variation enth. star 2
10.94726445953118   R(theta=0) [km] 
11.69389285127781   R(theta=pi/2, phi=0) [km] 
10.85260885026448   R(theta=pi/2, phi=pi/2) [km] 
11.60724265094991   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
68    step
0    abscidia of the rotation axis [km] 
2117.423071768156    Orbital frequency Omega [rad/s] 
0.01197205925401068    Abscidia ``center of mass'' star 1 [km] 
0.01197205925401068    Abscidia ``center of mass'' star 2 [km] 
1.527364853375632   Baryon mass of star 1  [M_sol] 
0.0004039513574021002   relative variation enth. star 1
10.94691786265062   R(theta=0) [km] 
11.69470454799272   R(theta=pi/2, phi=0) [km] 
10.85236909850643   R(theta=pi/2, phi=pi/2) [km] 
11.60717519125193   R(theta=pi/2, phi=pi) [km] 
1.527364853375632   Baryon mass of star 2  [M_sol] 
0.0004039513574021002   relative variation enth. star 2
10.94691786265062   R(theta=0) [km] 
11.69470454799272   R(theta=pi/2, phi=0) [km] 
10.85236909850643   R(theta=pi/2, phi=pi/2) [km] 
11.60717519125193   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
69    step
0    abscidia of the rotation axis [km] 
2117.363809740739    Orbital frequency Omega [rad/s] 
0.0120460658812882    Abscidia ``center of mass'' star 1 [km] 
0.0120460658812882    Abscidia ``center of mass'' star 2 [km] 
1.528306681160478   Baryon mass of star 1  [M_sol] 
0.0002224425211047398   relative variation enth. star 1
10.94938799514219   R(theta=0) [km] 
11.69772244967014   R(theta=pi/2, phi=0) [km] 
10.85485035068413   R(theta=pi/2, phi=pi/2) [km] 
11.60997801552553   R(theta=pi/2, phi=pi) [km] 
1.528306681160478   Baryon mass of star 2  [M_sol] 
0.0002224425211047398   relative variation enth. star 2
10.94938799514219   R(theta=0) [km] 
11.69772244967014   R(theta=pi/2, phi=0) [km] 
10.85485035068413   R(theta=pi/2, phi=pi/2) [km] 
11.60997801552553   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
70    step
0    abscidia of the rotation axis [km] 
2117.34252125173    Orbital frequency Omega [rad/s] 
0.01208210144896249    Abscidia ``center of mass'' star 1 [km] 
0.01208210144896249    Abscidia ``center of mass'' star 2 [km] 
1.528150241665152   Baryon mass of star 1  [M_sol] 
0.0002822328264463391   relative variation enth. star 1
10.94948329442321   R(theta=0) [km] 
11.69761840526088   R(theta=pi/2, phi=0) [km] 
10.85478468642616   R(theta=pi/2, phi=pi/2) [km] 
11.61017191503447   R(theta=pi/2, phi=pi) [km] 
1.528150241665152   Baryon mass of star 2  [M_sol] 
0.0002822328264463391   relative variation enth. star 2
10.94948329442321   R(theta=0) [km] 
11.69761840526088   R(theta=pi/2, phi=0) [km] 
10.85478468642616   R(theta=pi/2, phi=pi/2) [km] 
11.61017191503447   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
71    step
0    abscidia of the rotation axis [km] 
2117.293920738593    Orbital frequency Omega [rad/s] 
0.01201298995484601    Abscidia ``center of mass'' star 1 [km] 
0.01201298995484601    Abscidia ``center of mass'' star 2 [km] 
1.528042843983789   Baryon mass of star 1  [M_sol] 
0.0002848793513536195   relative variation enth. star 1
10.94960312283549   R(theta=0) [km] 
11.69791645941715   R(theta=pi/2, phi=0) [km] 
10.85487538067885   R(theta=pi/2, phi=pi/2) [km] 
11.61048324275534   R(theta=pi/2, phi=pi) [km] 
1.528042843983789   Baryon mass of star 2  [M_sol] 
0.0002848793513536195   relative variation enth. star 2
10.94960312283549   R(theta=0) [km] 
11.69791645941715   R(theta=pi/2, phi=0) [km] 
10.85487538067885   R(theta=pi/2, phi=pi/2) [km] 
11.61048324275534   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
72    step
0    abscidia of the rotation axis [km] 
2117.209464171779    Orbital frequency Omega [rad/s] 
0.01199342761519118    Abscidia ``center of mass'' star 1 [km] 
0.01199342761519118    Abscidia ``center of mass'' star 2 [km] 
1.527332971142665   Baryon mass of star 1  [M_sol] 
0.0002510863932918327   relative variation enth. star 1
10.94934802772965   R(theta=0) [km] 
11.69841673048028   R(theta=pi/2, phi=0) [km] 
10.85468864889589   R(theta=pi/2, phi=pi/2) [km] 
11.61041060843463   R(theta=pi/2, phi=pi) [km] 
1.527332971142665   Baryon mass of star 2  [M_sol] 
0.0002510863932918327   relative variation enth. star 2
10.94934802772965   R(theta=0) [km] 
11.69841673048028   R(theta=pi/2, phi=0) [km] 
10.85468864889589   R(theta=pi/2, phi=pi/2) [km] 
11.61041060843463   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
73    step
0    abscidia of the rotation axis [km] 
2117.18898598966    Orbital frequency Omega [rad/s] 
0.01204038012943442    Abscidia ``center of mass'' star 1 [km] 
0.01204038012943442    Abscidia ``center of mass'' star 2 [km] 
1.527884604821244   Baryon mass of star 1  [M_sol] 
0.0001408266389419635   relative variation enth. star 1
10.95080786309059   R(theta=0) [km] 
11.70025273684948   R(theta=pi/2, phi=0) [km] 
10.85614972907693   R(theta=pi/2, phi=pi/2) [km] 
11.61202689621427   R(theta=pi/2, phi=pi) [km] 
1.527884604821244   Baryon mass of star 2  [M_sol] 
0.0001408266389419635   relative variation enth. star 2
10.95080786309059   R(theta=0) [km] 
11.70025273684948   R(theta=pi/2, phi=0) [km] 
10.85614972907693   R(theta=pi/2, phi=pi/2) [km] 
11.61202689621427   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
74    step
0    abscidia of the rotation axis [km] 
2117.214582427265    Orbital frequency Omega [rad/s] 
0.01209079456769402    Abscidia ``center of mass'' star 1 [km] 
0.01209079456769402    Abscidia ``center of mass'' star 2 [km] 
1.527800821617403   Baryon mass of star 1  [M_sol] 
0.0001748592122452289   relative variation enth. star 1
10.95090239771839   R(theta=0) [km] 
11.70020351616238   R(theta=pi/2, phi=0) [km] 
10.8561342214876   R(theta=pi/2, phi=pi/2) [km] 
11.61219307614166   R(theta=pi/2, phi=pi) [km] 
1.527800821617403   Baryon mass of star 2  [M_sol] 
0.0001748592122452289   relative variation enth. star 2
10.95090239771839   R(theta=0) [km] 
11.70020351616238   R(theta=pi/2, phi=0) [km] 
10.8561342214876   R(theta=pi/2, phi=pi/2) [km] 
11.61219307614166   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
75    step
0    abscidia of the rotation axis [km] 
2117.212350082985    Orbital frequency Omega [rad/s] 
0.01204519154306194    Abscidia ``center of mass'' star 1 [km] 
0.01204519154306194    Abscidia ``center of mass'' star 2 [km] 
1.527725190513563   Baryon mass of star 1  [M_sol] 
0.000176349774776281   relative variation enth. star 1
10.95095914405177   R(theta=0) [km] 
11.70034232480903   R(theta=pi/2, phi=0) [km] 
10.85616334595683   R(theta=pi/2, phi=pi/2) [km] 
11.61238194753253   R(theta=pi/2, phi=pi) [km] 
1.527725190513563   Baryon mass of star 2  [M_sol] 
0.000176349774776281   relative variation enth. star 2
10.95095914405177   R(theta=0) [km] 
11.70034232480903   R(theta=pi/2, phi=0) [km] 
10.85616334595683   R(theta=pi/2, phi=pi/2) [km] 
11.61238194753253   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
76    step
0    abscidia of the rotation axis [km] 
2117.175547762724    Orbital frequency Omega [rad/s] 
0.01202490739568018    Abscidia ``center of mass'' star 1 [km] 
0.01202490739568018    Abscidia ``center of mass'' star 2 [km] 
1.527280009221395   Baryon mass of star 1  [M_sol] 
0.000154179513842808   relative variation enth. star 1
10.95081544364278   R(theta=0) [km] 
11.70067519316708   R(theta=pi/2, phi=0) [km] 
10.85606013700554   R(theta=pi/2, phi=pi/2) [km] 
11.61234605981006   R(theta=pi/2, phi=pi) [km] 
1.527280009221395   Baryon mass of star 2  [M_sol] 
0.000154179513842808   relative variation enth. star 2
10.95081544364278   R(theta=0) [km] 
11.70067519316708   R(theta=pi/2, phi=0) [km] 
10.85606013700554   R(theta=pi/2, phi=pi/2) [km] 
11.61234605981006   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
77    step
0    abscidia of the rotation axis [km] 
2117.152976435723    Orbital frequency Omega [rad/s] 
0.01205357483570868    Abscidia ``center of mass'' star 1 [km] 
0.01205357483570868    Abscidia ``center of mass'' star 2 [km] 
1.527643112486012   Baryon mass of star 1  [M_sol] 
8.4987318016413e-05   relative variation enth. star 1
10.95176707294542   R(theta=0) [km] 
11.70184601429179   R(theta=pi/2, phi=0) [km] 
10.8570156594983   R(theta=pi/2, phi=pi/2) [km] 
11.6134300600107   R(theta=pi/2, phi=pi) [km] 
1.527643112486012   Baryon mass of star 2  [M_sol] 
8.4987318016413e-05   relative variation enth. star 2
10.95176707294542   R(theta=0) [km] 
11.70184601429179   R(theta=pi/2, phi=0) [km] 
10.8570156594983   R(theta=pi/2, phi=pi/2) [km] 
11.6134300600107   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
78    step
0    abscidia of the rotation axis [km] 
2117.142811735394    Orbital frequency Omega [rad/s] 
0.01206627525027315    Abscidia ``center of mass'' star 1 [km] 
0.01206627525027315    Abscidia ``center of mass'' star 2 [km] 
1.527584612933152   Baryon mass of star 1  [M_sol] 
0.0001081961017802522   relative variation enth. star 1
10.95180771906815   R(theta=0) [km] 
11.70181067116518   R(theta=pi/2, phi=0) [km] 
10.85699459347488   R(theta=pi/2, phi=pi/2) [km] 
11.61350994977935   R(theta=pi/2, phi=pi) [km] 
1.527584612933152   Baryon mass of star 2  [M_sol] 
0.0001081961017802522   relative variation enth. star 2
10.95180771906815   R(theta=0) [km] 
11.70181067116518   R(theta=pi/2, phi=0) [km] 
10.85699459347488   R(theta=pi/2, phi=pi/2) [km] 
11.61350994977935   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
79    step
0    abscidia of the rotation axis [km] 
2117.12130643875    Orbital frequency Omega [rad/s] 
0.01203895345456729    Abscidia ``center of mass'' star 1 [km] 
0.01203895345456729    Abscidia ``center of mass'' star 2 [km] 
1.527543779890259   Baryon mass of star 1  [M_sol] 
0.0001093928713794469   relative variation enth. star 1
10.95185522402588   R(theta=0) [km] 
11.70193175117758   R(theta=pi/2, phi=0) [km] 
10.85703087806401   R(theta=pi/2, phi=pi/2) [km] 
11.61363026496915   R(theta=pi/2, phi=pi) [km] 
1.527543779890259   Baryon mass of star 2  [M_sol] 
0.0001093928713794469   relative variation enth. star 2
10.95185522402588   R(theta=0) [km] 
11.70193175117758   R(theta=pi/2, phi=0) [km] 
10.85703087806401   R(theta=pi/2, phi=pi/2) [km] 
11.61363026496915   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
80    step
0    abscidia of the rotation axis [km] 
2117.088941404667    Orbital frequency Omega [rad/s] 
0.01203136748091138    Abscidia ``center of mass'' star 1 [km] 
0.01203136748091138    Abscidia ``center of mass'' star 2 [km] 
1.527270305642727   Baryon mass of star 1  [M_sol] 
9.576867984380383e-05   relative variation enth. star 1
10.95174877839143   R(theta=0) [km] 
11.70213433197434   R(theta=pi/2, phi=0) [km] 
10.85695012381951   R(theta=pi/2, phi=pi/2) [km] 
11.61358468768428   R(theta=pi/2, phi=pi) [km] 
1.527270305642727   Baryon mass of star 2  [M_sol] 
9.576867984380383e-05   relative variation enth. star 2
10.95174877839143   R(theta=0) [km] 
11.70213433197434   R(theta=pi/2, phi=0) [km] 
10.85695012381951   R(theta=pi/2, phi=pi/2) [km] 
11.61358468768428   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
81    step
0    abscidia of the rotation axis [km] 
2117.081790108813    Orbital frequency Omega [rad/s] 
0.01205319821169226    Abscidia ``center of mass'' star 1 [km] 
0.01205319821169226    Abscidia ``center of mass'' star 2 [km] 
1.52748242728013   Baryon mass of star 1  [M_sol] 
5.402288101762336e-05   relative variation enth. star 1
10.95231017077421   R(theta=0) [km] 
11.70284410321906   R(theta=pi/2, phi=0) [km] 
10.85751162514854   R(theta=pi/2, phi=pi/2) [km] 
11.61420789596998   R(theta=pi/2, phi=pi) [km] 
1.52748242728013   Baryon mass of star 2  [M_sol] 
5.402288101762336e-05   relative variation enth. star 2
10.95231017077421   R(theta=0) [km] 
11.70284410321906   R(theta=pi/2, phi=0) [km] 
10.85751162514854   R(theta=pi/2, phi=pi/2) [km] 
11.61420789596998   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
82    step
0    abscidia of the rotation axis [km] 
2117.093542271648    Orbital frequency Omega [rad/s] 
0.01207220728052016    Abscidia ``center of mass'' star 1 [km] 
0.01207220728052016    Abscidia ``center of mass'' star 2 [km] 
1.527451182925377   Baryon mass of star 1  [M_sol] 
6.714568240220028e-05   relative variation enth. star 1
10.95234915011499   R(theta=0) [km] 
11.70282523801136   R(theta=pi/2, phi=0) [km] 
10.85750789213159   R(theta=pi/2, phi=pi/2) [km] 
11.614276445269   R(theta=pi/2, phi=pi) [km] 
1.527451182925377   Baryon mass of star 2  [M_sol] 
6.714568240220028e-05   relative variation enth. star 2
10.95234915011499   R(theta=0) [km] 
11.70282523801136   R(theta=pi/2, phi=0) [km] 
10.85750789213159   R(theta=pi/2, phi=pi/2) [km] 
11.614276445269   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
83    step
0    abscidia of the rotation axis [km] 
2117.09354041729    Orbital frequency Omega [rad/s] 
0.01205368526474571    Abscidia ``center of mass'' star 1 [km] 
0.01205368526474571    Abscidia ``center of mass'' star 2 [km] 
1.52742195774848   Baryon mass of star 1  [M_sol] 
6.781786371749545e-05   relative variation enth. star 1
10.95237102579335   R(theta=0) [km] 
11.70287910902964   R(theta=pi/2, phi=0) [km] 
10.85751854720099   R(theta=pi/2, phi=pi/2) [km] 
11.61435001599846   R(theta=pi/2, phi=pi) [km] 
1.52742195774848   Baryon mass of star 2  [M_sol] 
6.781786371749545e-05   relative variation enth. star 2
10.95237102579335   R(theta=0) [km] 
11.70287910902964   R(theta=pi/2, phi=0) [km] 
10.85751854720099   R(theta=pi/2, phi=pi/2) [km] 
11.61435001599846   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
84    step
0    abscidia of the rotation axis [km] 
2117.080031219789    Orbital frequency Omega [rad/s] 
0.01204533912889216    Abscidia ``center of mass'' star 1 [km] 
0.01204533912889216    Abscidia ``center of mass'' star 2 [km] 
1.527250399780089   Baryon mass of star 1  [M_sol] 
5.905123849431338e-05   relative variation enth. star 1
10.95231298045996   R(theta=0) [km] 
11.70301191353223   R(theta=pi/2, phi=0) [km] 
10.85747582127612   R(theta=pi/2, phi=pi/2) [km] 
11.61433115050218   R(theta=pi/2, phi=pi) [km] 
1.527250399780089   Baryon mass of star 2  [M_sol] 
5.905123849431338e-05   relative variation enth. star 2
10.95231298045996   R(theta=0) [km] 
11.70301191353223   R(theta=pi/2, phi=0) [km] 
10.85747582127612   R(theta=pi/2, phi=pi/2) [km] 
11.61433115050218   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
85    step
0    abscidia of the rotation axis [km] 
2117.071410373118    Orbital frequency Omega [rad/s] 
0.01205725925599754    Abscidia ``center of mass'' star 1 [km] 
0.01205725925599754    Abscidia ``center of mass'' star 2 [km] 
1.527390586592511   Baryon mass of star 1  [M_sol] 
3.265126783967489e-05   relative variation enth. star 1
10.95268020574459   R(theta=0) [km] 
11.70346506860433   R(theta=pi/2, phi=0) [km] 
10.85784451751206   R(theta=pi/2, phi=pi/2) [km] 
11.61475087860224   R(theta=pi/2, phi=pi) [km] 
1.527390586592511   Baryon mass of star 2  [M_sol] 
3.265126783967489e-05   relative variation enth. star 2
10.95268020574459   R(theta=0) [km] 
11.70346506860433   R(theta=pi/2, phi=0) [km] 
10.85784451751206   R(theta=pi/2, phi=pi/2) [km] 
11.61475087860224   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
86    step
0    abscidia of the rotation axis [km] 
2117.067303056643    Orbital frequency Omega [rad/s] 
0.01206152783014502    Abscidia ``center of mass'' star 1 [km] 
0.01206152783014502    Abscidia ``center of mass'' star 2 [km] 
1.527368368243625   Baryon mass of star 1  [M_sol] 
4.165714523431737e-05   relative variation enth. star 1
10.95269665282353   R(theta=0) [km] 
11.70345215702959   R(theta=pi/2, phi=0) [km] 
10.85783738408606   R(theta=pi/2, phi=pi/2) [km] 
11.61478311440605   R(theta=pi/2, phi=pi) [km] 
1.527368368243625   Baryon mass of star 2  [M_sol] 
4.165714523431737e-05   relative variation enth. star 2
10.95269665282353   R(theta=0) [km] 
11.70345215702959   R(theta=pi/2, phi=0) [km] 
10.85783738408606   R(theta=pi/2, phi=pi/2) [km] 
11.61478311440605   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
87    step
0    abscidia of the rotation axis [km] 
2117.058635410608    Orbital frequency Omega [rad/s] 
0.01205074339295287    Abscidia ``center of mass'' star 1 [km] 
0.01205074339295287    Abscidia ``center of mass'' star 2 [km] 
1.527352588384701   Baryon mass of star 1  [M_sol] 
4.21398031697891e-05   relative variation enth. star 1
10.95271489643885   R(theta=0) [km] 
11.70349960625235   R(theta=pi/2, phi=0) [km] 
10.85785128592042   R(theta=pi/2, phi=pi/2) [km] 
11.61482928968731   R(theta=pi/2, phi=pi) [km] 
1.527352588384701   Baryon mass of star 2  [M_sol] 
4.21398031697891e-05   relative variation enth. star 2
10.95271489643885   R(theta=0) [km] 
11.70349960625235   R(theta=pi/2, phi=0) [km] 
10.85785128592042   R(theta=pi/2, phi=pi/2) [km] 
11.61482928968731   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
88    step
0    abscidia of the rotation axis [km] 
2117.045444860362    Orbital frequency Omega [rad/s] 
0.01204776559002863    Abscidia ``center of mass'' star 1 [km] 
0.01204776559002863    Abscidia ``center of mass'' star 2 [km] 
1.527247411138115   Baryon mass of star 1  [M_sol] 
3.682282096996239e-05   relative variation enth. star 1
10.95267283526572   R(theta=0) [km] 
11.70357939504131   R(theta=pi/2, phi=0) [km] 
10.85781927068734   R(theta=pi/2, phi=pi/2) [km] 
11.61481122769424   R(theta=pi/2, phi=pi) [km] 
1.527247411138115   Baryon mass of star 2  [M_sol] 
3.682282096996239e-05   relative variation enth. star 2
10.95267283526572   R(theta=0) [km] 
11.70357939504131   R(theta=pi/2, phi=0) [km] 
10.85781927068734   R(theta=pi/2, phi=pi/2) [km] 
11.61481122769424   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
89    step
0    abscidia of the rotation axis [km] 
2117.042668172958    Orbital frequency Omega [rad/s] 
0.01205602882621726    Abscidia ``center of mass'' star 1 [km] 
0.01205602882621726    Abscidia ``center of mass'' star 2 [km] 
1.527328684234837   Baryon mass of star 1  [M_sol] 
2.082207564039118e-05   relative variation enth. star 1
10.95288806954261   R(theta=0) [km] 
11.70385350110104   R(theta=pi/2, phi=0) [km] 
10.85803448104544   R(theta=pi/2, phi=pi/2) [km] 
11.61504925469467   R(theta=pi/2, phi=pi) [km] 
1.527328684234837   Baryon mass of star 2  [M_sol] 
2.082207564039118e-05   relative variation enth. star 2
10.95288806954261   R(theta=0) [km] 
11.70385350110104   R(theta=pi/2, phi=0) [km] 
10.85803448104544   R(theta=pi/2, phi=pi/2) [km] 
11.61504925469467   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
90    step
0    abscidia of the rotation axis [km] 
2117.047674006969    Orbital frequency Omega [rad/s] 
0.01206397569725626    Abscidia ``center of mass'' star 1 [km] 
0.01206397569725626    Abscidia ``center of mass'' star 2 [km] 
1.527317065834296   Baryon mass of star 1  [M_sol] 
2.587496173232306e-05   relative variation enth. star 1
10.95290424146244   R(theta=0) [km] 
11.70384739654468   R(theta=pi/2, phi=0) [km] 
10.85803406501138   R(theta=pi/2, phi=pi/2) [km] 
11.6150770210399   R(theta=pi/2, phi=pi) [km] 
1.527317065834296   Baryon mass of star 2  [M_sol] 
2.587496173232306e-05   relative variation enth. star 2
10.95290424146244   R(theta=0) [km] 
11.70384739654468   R(theta=pi/2, phi=0) [km] 
10.85803406501138   R(theta=pi/2, phi=pi/2) [km] 
11.6150770210399   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
91    step
0    abscidia of the rotation axis [km] 
2117.048191436852    Orbital frequency Omega [rad/s] 
0.01205691296226163    Abscidia ``center of mass'' star 1 [km] 
0.01205691296226163    Abscidia ``center of mass'' star 2 [km] 
1.527305624488059   Baryon mass of star 1  [M_sol] 
2.61516847169441e-05   relative variation enth. star 1
10.95291244883019   R(theta=0) [km] 
11.7038675751282   R(theta=pi/2, phi=0) [km] 
10.85803768729527   R(theta=pi/2, phi=pi/2) [km] 
11.61510534204296   R(theta=pi/2, phi=pi) [km] 
1.527305624488059   Baryon mass of star 2  [M_sol] 
2.61516847169441e-05   relative variation enth. star 2
10.95291244883019   R(theta=0) [km] 
11.7038675751282   R(theta=pi/2, phi=0) [km] 
10.85803768729527   R(theta=pi/2, phi=pi/2) [km] 
11.61510534204296   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
92    step
0    abscidia of the rotation axis [km] 
2117.042886893577    Orbital frequency Omega [rad/s] 
0.01205360101278696    Abscidia ``center of mass'' star 1 [km] 
0.01205360101278696    Abscidia ``center of mass'' star 2 [km] 
1.527239526352716   Baryon mass of star 1  [M_sol] 
2.272926091211893e-05   relative variation enth. star 1
10.95288994329034   R(theta=0) [km] 
11.70391940197439   R(theta=pi/2, phi=0) [km] 
10.85802110701371   R(theta=pi/2, phi=pi/2) [km] 
11.6150987206267   R(theta=pi/2, phi=pi) [km] 
1.527239526352716   Baryon mass of star 2  [M_sol] 
2.272926091211893e-05   relative variation enth. star 2
10.95288994329034   R(theta=0) [km] 
11.70391940197439   R(theta=pi/2, phi=0) [km] 
10.85802110701371   R(theta=pi/2, phi=pi/2) [km] 
11.6150987206267   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
93    step
0    abscidia of the rotation axis [km] 
2117.039453023446    Orbital frequency Omega [rad/s] 
0.01205793304117186    Abscidia ``center of mass'' star 1 [km] 
0.01205793304117186    Abscidia ``center of mass'' star 2 [km] 
1.527293566671275   Baryon mass of star 1  [M_sol] 
1.256245716629434e-05   relative variation enth. star 1
10.95303147801433   R(theta=0) [km] 
11.7040943687517   R(theta=pi/2, phi=0) [km] 
10.85816319491473   R(theta=pi/2, phi=pi/2) [km] 
11.61526072009194   R(theta=pi/2, phi=pi) [km] 
1.527293566671275   Baryon mass of star 2  [M_sol] 
1.256245716629434e-05   relative variation enth. star 2
10.95303147801433   R(theta=0) [km] 
11.7040943687517   R(theta=pi/2, phi=0) [km] 
10.85816319491473   R(theta=pi/2, phi=pi/2) [km] 
11.61526072009194   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
94    step
0    abscidia of the rotation axis [km] 
2117.037611253271    Orbital frequency Omega [rad/s] 
0.01205957218551879    Abscidia ``center of mass'' star 1 [km] 
0.01205957218551879    Abscidia ``center of mass'' star 2 [km] 
1.527285050215341   Baryon mass of star 1  [M_sol] 
1.603851891720625e-05   relative variation enth. star 1
10.95303791045351   R(theta=0) [km] 
11.70408966938141   R(theta=pi/2, phi=0) [km] 
10.85816059789155   R(theta=pi/2, phi=pi/2) [km] 
11.61527331815774   R(theta=pi/2, phi=pi) [km] 
1.527285050215341   Baryon mass of star 2  [M_sol] 
1.603851891720625e-05   relative variation enth. star 2
10.95303791045351   R(theta=0) [km] 
11.70408966938141   R(theta=pi/2, phi=0) [km] 
10.85816059789155   R(theta=pi/2, phi=pi/2) [km] 
11.61527331815774   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
95    step
0    abscidia of the rotation axis [km] 
2117.034106636806    Orbital frequency Omega [rad/s] 
0.01205548255197142    Abscidia ``center of mass'' star 1 [km] 
0.01205548255197142    Abscidia ``center of mass'' star 2 [km] 
1.527278982464732   Baryon mass of star 1  [M_sol] 
1.622410342991269e-05   relative variation enth. star 1
10.95304496940275   R(theta=0) [km] 
11.70410809559985   R(theta=pi/2, phi=0) [km] 
10.85816598172332   R(theta=pi/2, phi=pi/2) [km] 
11.61529121846873   R(theta=pi/2, phi=pi) [km] 
1.527278982464732   Baryon mass of star 2  [M_sol] 
1.622410342991269e-05   relative variation enth. star 2
10.95304496940275   R(theta=0) [km] 
11.70410809559985   R(theta=pi/2, phi=0) [km] 
10.85816598172332   R(theta=pi/2, phi=pi/2) [km] 
11.61529121846873   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
96    step
0    abscidia of the rotation axis [km] 
2117.028859711878    Orbital frequency Omega [rad/s] 
0.01205438075177812    Abscidia ``center of mass'' star 1 [km] 
0.01205438075177812    Abscidia ``center of mass'' star 2 [km] 
1.52723851765626   Baryon mass of star 1  [M_sol] 
1.416717574259464e-05   relative variation enth. star 1
10.95302864164051   R(theta=0) [km] 
11.70413909493185   R(theta=pi/2, phi=0) [km] 
10.85815353063044   R(theta=pi/2, phi=pi/2) [km] 
11.61528438276049   R(theta=pi/2, phi=pi) [km] 
1.52723851765626   Baryon mass of star 2  [M_sol] 
1.416717574259464e-05   relative variation enth. star 2
10.95302864164051   R(theta=0) [km] 
11.70413909493185   R(theta=pi/2, phi=0) [km] 
10.85815353063044   R(theta=pi/2, phi=pi/2) [km] 
11.61528438276049   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
97    step
0    abscidia of the rotation axis [km] 
2117.027798353177    Orbital frequency Omega [rad/s] 
0.01205755829597033    Abscidia ``center of mass'' star 1 [km] 
0.01205755829597033    Abscidia ``center of mass'' star 2 [km] 
1.527269735314286   Baryon mass of star 1  [M_sol] 
8.020339014560502e-06   relative variation enth. star 1
10.95311136216193   R(theta=0) [km] 
11.70424459795876   R(theta=pi/2, phi=0) [km] 
10.85823618296432   R(theta=pi/2, phi=pi/2) [km] 
11.61537593682321   R(theta=pi/2, phi=pi) [km] 
1.527269735314286   Baryon mass of star 2  [M_sol] 
8.020339014560502e-06   relative variation enth. star 2
10.95311136216193   R(theta=0) [km] 
11.70424459795876   R(theta=pi/2, phi=0) [km] 
10.85823618296432   R(theta=pi/2, phi=pi/2) [km] 
11.61537593682321   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
98    step
0    abscidia of the rotation axis [km] 
2117.029841931532    Orbital frequency Omega [rad/s] 
0.01206073769686178    Abscidia ``center of mass'' star 1 [km] 
0.01206073769686178    Abscidia ``center of mass'' star 2 [km] 
1.527265280163446   Baryon mass of star 1  [M_sol] 
9.956102669601307e-06   relative variation enth. star 1
10.95311765110698   R(theta=0) [km] 
11.70424212861096   R(theta=pi/2, phi=0) [km] 
10.85823601795492   R(theta=pi/2, phi=pi/2) [km] 
11.61538689454297   R(theta=pi/2, phi=pi) [km] 
1.527265280163446   Baryon mass of star 2  [M_sol] 
9.956102669601307e-06   relative variation enth. star 2
10.95311765110698   R(theta=0) [km] 
11.70424212861096   R(theta=pi/2, phi=0) [km] 
10.85823601795492   R(theta=pi/2, phi=pi/2) [km] 
11.61538689454297   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
99    step
0    abscidia of the rotation axis [km] 
2117.030153563736    Orbital frequency Omega [rad/s] 
0.01205802407648671    Abscidia ``center of mass'' star 1 [km] 
0.01205802407648671    Abscidia ``center of mass'' star 2 [km] 
1.527260843583055   Baryon mass of star 1  [M_sol] 
1.00566986739032e-05   relative variation enth. star 1
10.95312073958571   R(theta=0) [km] 
11.70424959474598   R(theta=pi/2, phi=0) [km] 
10.8582372665369   R(theta=pi/2, phi=pi/2) [km] 
11.61539801997559   R(theta=pi/2, phi=pi) [km] 
1.527260843583055   Baryon mass of star 2  [M_sol] 
1.00566986739032e-05   relative variation enth. star 2
10.95312073958571   R(theta=0) [km] 
11.70424959474598   R(theta=pi/2, phi=0) [km] 
10.8582372665369   R(theta=pi/2, phi=pi/2) [km] 
11.61539801997559   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
100    step
0    abscidia of the rotation axis [km] 
2117.028189073706    Orbital frequency Omega [rad/s] 
0.01205673059972545    Abscidia ``center of mass'' star 1 [km] 
0.01205673059972545    Abscidia ``center of mass'' star 2 [km] 
1.527235432169341   Baryon mass of star 1  [M_sol] 
8.740589498592215e-06   relative variation enth. star 1
10.95311220919165   R(theta=0) [km] 
11.70426979177653   R(theta=pi/2, phi=0) [km] 
10.85823097198672   R(theta=pi/2, phi=pi/2) [km] 
11.61539577789924   R(theta=pi/2, phi=pi) [km] 
1.527235432169341   Baryon mass of star 2  [M_sol] 
8.740589498592215e-06   relative variation enth. star 2
10.95311220919165   R(theta=0) [km] 
11.70426979177653   R(theta=pi/2, phi=0) [km] 
10.85823097198672   R(theta=pi/2, phi=pi/2) [km] 
11.61539577789924   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
101    step
0    abscidia of the rotation axis [km] 
2117.026851943519    Orbital frequency Omega [rad/s] 
0.01205841411620057    Abscidia ``center of mass'' star 1 [km] 
0.01205841411620057    Abscidia ``center of mass'' star 2 [km] 
1.527256295935992   Baryon mass of star 1  [M_sol] 
4.832769998567384e-06   relative variation enth. star 1
10.95316684917647   R(theta=0) [km] 
11.70433710139234   R(theta=pi/2, phi=0) [km] 
10.85828577339503   R(theta=pi/2, phi=pi/2) [km] 
11.61545872512696   R(theta=pi/2, phi=pi) [km] 
1.527256295935992   Baryon mass of star 2  [M_sol] 
4.832769998567384e-06   relative variation enth. star 2
10.95316684917647   R(theta=0) [km] 
11.70433710139234   R(theta=pi/2, phi=0) [km] 
10.85828577339503   R(theta=pi/2, phi=pi/2) [km] 
11.61545872512696   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
102    step
0    abscidia of the rotation axis [km] 
2117.026057859126    Orbital frequency Omega [rad/s] 
0.01205896303926979    Abscidia ``center of mass'' star 1 [km] 
0.01205896303926979    Abscidia ``center of mass'' star 2 [km] 
1.527252950314444   Baryon mass of star 1  [M_sol] 
6.171397649783453e-06   relative variation enth. star 1
10.95316915396637   R(theta=0) [km] 
11.7043350247867   R(theta=pi/2, phi=0) [km] 
10.85828457885114   R(theta=pi/2, phi=pi/2) [km] 
11.61546360890794   R(theta=pi/2, phi=pi) [km] 
1.527252950314444   Baryon mass of star 2  [M_sol] 
6.171397649783453e-06   relative variation enth. star 2
10.95316915396637   R(theta=0) [km] 
11.7043350247867   R(theta=pi/2, phi=0) [km] 
10.85828457885114   R(theta=pi/2, phi=pi/2) [km] 
11.61546360890794   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
103    step
0    abscidia of the rotation axis [km] 
2117.024630679935    Orbital frequency Omega [rad/s] 
0.01205738127408207    Abscidia ``center of mass'' star 1 [km] 
0.01205738127408207    Abscidia ``center of mass'' star 2 [km] 
1.527250658547673   Baryon mass of star 1  [M_sol] 
6.239502410931103e-06   relative variation enth. star 1
10.95317195636255   R(theta=0) [km] 
11.70434218976315   R(theta=pi/2, phi=0) [km] 
10.85828671134037   R(theta=pi/2, phi=pi/2) [km] 
11.61547084297985   R(theta=pi/2, phi=pi) [km] 
1.527250658547673   Baryon mass of star 2  [M_sol] 
6.239502410931103e-06   relative variation enth. star 2
10.95317195636255   R(theta=0) [km] 
11.70434218976315   R(theta=pi/2, phi=0) [km] 
10.85828671134037   R(theta=pi/2, phi=pi/2) [km] 
11.61547084297985   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
104    step
0    abscidia of the rotation axis [km] 
2117.02259115366    Orbital frequency Omega [rad/s] 
0.01205697273251927    Abscidia ``center of mass'' star 1 [km] 
0.01205697273251927    Abscidia ``center of mass'' star 2 [km] 
1.527235129872474   Baryon mass of star 1  [M_sol] 
5.45611397444206e-06   relative variation enth. star 1
10.95316569592146   R(theta=0) [km] 
11.70435417865398   R(theta=pi/2, phi=0) [km] 
10.85828190113235   R(theta=pi/2, phi=pi/2) [km] 
11.61546839209378   R(theta=pi/2, phi=pi) [km] 
1.527235129872474   Baryon mass of star 2  [M_sol] 
5.45611397444206e-06   relative variation enth. star 2
10.95316569592146   R(theta=0) [km] 
11.70435417865398   R(theta=pi/2, phi=0) [km] 
10.85828190113235   R(theta=pi/2, phi=pi/2) [km] 
11.61546839209378   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
105    step
0    abscidia of the rotation axis [km] 
2117.02220542417    Orbital frequency Omega [rad/s] 
0.01205823125168148    Abscidia ``center of mass'' star 1 [km] 
0.01205823125168148    Abscidia ``center of mass'' star 2 [km] 
1.527247075865061   Baryon mass of star 1  [M_sol] 
3.095793447203251e-06   relative variation enth. star 1
10.9531973928978   R(theta=0) [km] 
11.70439452947829   R(theta=pi/2, phi=0) [km] 
10.85831349090651   R(theta=pi/2, phi=pi/2) [km] 
11.61550369133565   R(theta=pi/2, phi=pi) [km] 
1.527247075865061   Baryon mass of star 2  [M_sol] 
3.095793447203251e-06   relative variation enth. star 2
10.9531973928978   R(theta=0) [km] 
11.70439452947829   R(theta=pi/2, phi=0) [km] 
10.85831349090651   R(theta=pi/2, phi=pi/2) [km] 
11.61550369133565   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
106    step
0    abscidia of the rotation axis [km] 
2117.023056645093    Orbital frequency Omega [rad/s] 
0.0120594823138509    Abscidia ``center of mass'' star 1 [km] 
0.0120594823138509    Abscidia ``center of mass'' star 2 [km] 
1.527245344851076   Baryon mass of star 1  [M_sol] 
3.828360470853035e-06   relative variation enth. star 1
10.95319977306315   R(theta=0) [km] 
11.70439336864668   R(theta=pi/2, phi=0) [km] 
10.85831330583675   R(theta=pi/2, phi=pi/2) [km] 
11.61550812499858   R(theta=pi/2, phi=pi) [km] 
1.527245344851076   Baryon mass of star 2  [M_sol] 
3.828360470853035e-06   relative variation enth. star 2
10.95319977306315   R(theta=0) [km] 
11.70439336864668   R(theta=pi/2, phi=0) [km] 
10.85831330583675   R(theta=pi/2, phi=pi/2) [km] 
11.61550812499858   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
107    step
0    abscidia of the rotation axis [km] 
2117.023226276225    Orbital frequency Omega [rad/s] 
0.01205842841492721    Abscidia ``center of mass'' star 1 [km] 
0.01205842841492721    Abscidia ``center of mass'' star 2 [km] 
1.527243625213202   Baryon mass of star 1  [M_sol] 
3.860639516405949e-06   relative variation enth. star 1
10.9532009197659   R(theta=0) [km] 
11.70439602198027   R(theta=pi/2, phi=0) [km] 
10.85831366193846   R(theta=pi/2, phi=pi/2) [km] 
11.61551266550341   R(theta=pi/2, phi=pi) [km] 
1.527243625213202   Baryon mass of star 2  [M_sol] 
3.860639516405949e-06   relative variation enth. star 2
10.9532009197659   R(theta=0) [km] 
11.70439602198027   R(theta=pi/2, phi=0) [km] 
10.85831366193846   R(theta=pi/2, phi=pi/2) [km] 
11.61551266550341   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
108    step
0    abscidia of the rotation axis [km] 
2117.022484426703    Orbital frequency Omega [rad/s] 
0.01205791743656315    Abscidia ``center of mass'' star 1 [km] 
0.01205791743656315    Abscidia ``center of mass'' star 2 [km] 
1.527233864910536   Baryon mass of star 1  [M_sol] 
3.358847646251692e-06   relative variation enth. star 1
10.95319773916285   R(theta=0) [km] 
11.70440374640458   R(theta=pi/2, phi=0) [km] 
10.85831129062183   R(theta=pi/2, phi=pi/2) [km] 
11.61551216994891   R(theta=pi/2, phi=pi) [km] 
1.527233864910536   Baryon mass of star 2  [M_sol] 
3.358847646251692e-06   relative variation enth. star 2
10.95319773916285   R(theta=0) [km] 
11.70440374640458   R(theta=pi/2, phi=0) [km] 
10.85831129062183   R(theta=pi/2, phi=pi/2) [km] 
11.61551216994891   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
109    step
0    abscidia of the rotation axis [km] 
2117.021949397174    Orbital frequency Omega [rad/s] 
0.0120585514774052    Abscidia ``center of mass'' star 1 [km] 
0.0120585514774052    Abscidia ``center of mass'' star 2 [km] 
1.527241925286772   Baryon mass of star 1  [M_sol] 
1.853518934603763e-06   relative variation enth. star 1
10.95321884177818   R(theta=0) [km] 
11.70442956980937   R(theta=pi/2, phi=0) [km] 
10.85833236498335   R(theta=pi/2, phi=pi/2) [km] 
11.61553677565837   R(theta=pi/2, phi=pi) [km] 
1.527241925286772   Baryon mass of star 2  [M_sol] 
1.853518934603763e-06   relative variation enth. star 2
10.95321884177818   R(theta=0) [km] 
11.70442956980937   R(theta=pi/2, phi=0) [km] 
10.85833236498335   R(theta=pi/2, phi=pi/2) [km] 
11.61553677565837   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
110    step
0    abscidia of the rotation axis [km] 
2117.021603911197    Orbital frequency Omega [rad/s] 
0.01205874261404727    Abscidia ``center of mass'' star 1 [km] 
0.01205874261404727    Abscidia ``center of mass'' star 2 [km] 
1.527240527300706   Baryon mass of star 1  [M_sol] 
2.354570213273878e-06   relative variation enth. star 1
10.95321942160591   R(theta=0) [km] 
11.70442824790853   R(theta=pi/2, phi=0) [km] 
10.85833152524801   R(theta=pi/2, phi=pi/2) [km] 
11.61553855956862   R(theta=pi/2, phi=pi) [km] 
1.527240527300706   Baryon mass of star 2  [M_sol] 
2.354570213273878e-06   relative variation enth. star 2
10.95321942160591   R(theta=0) [km] 
11.70442824790853   R(theta=pi/2, phi=0) [km] 
10.85833152524801   R(theta=pi/2, phi=pi/2) [km] 
11.61553855956862   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
111    step
0    abscidia of the rotation axis [km] 
2117.021039846056    Orbital frequency Omega [rad/s] 
0.0120581416783172    Abscidia ``center of mass'' star 1 [km] 
0.0120581416783172    Abscidia ``center of mass'' star 2 [km] 
1.527239745335081   Baryon mass of star 1  [M_sol] 
2.381137047634323e-06   relative variation enth. star 1
10.95322070091758   R(theta=0) [km] 
11.70443118554924   R(theta=pi/2, phi=0) [km] 
10.85833246484409   R(theta=pi/2, phi=pi/2) [km] 
11.61554189119195   R(theta=pi/2, phi=pi) [km] 
1.527239745335081   Baryon mass of star 2  [M_sol] 
2.381137047634323e-06   relative variation enth. star 2
10.95322070091758   R(theta=0) [km] 
11.70443118554924   R(theta=pi/2, phi=0) [km] 
10.85833246484409   R(theta=pi/2, phi=pi/2) [km] 
11.61554189119195   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
112    step
0    abscidia of the rotation axis [km] 
2117.020249363389    Orbital frequency Omega [rad/s] 
0.01205799625956239    Abscidia ``center of mass'' star 1 [km] 
0.01205799625956239    Abscidia ``center of mass'' star 2 [km] 
1.527233788242685   Baryon mass of star 1  [M_sol] 
2.093023369417965e-06   relative variation enth. star 1
10.95321825913403   R(theta=0) [km] 
11.70443553405696   R(theta=pi/2, phi=0) [km] 
10.85833050326065   R(theta=pi/2, phi=pi/2) [km] 
11.61554118466738   R(theta=pi/2, phi=pi) [km] 
1.527233788242685   Baryon mass of star 2  [M_sol] 
2.093023369417965e-06   relative variation enth. star 2
10.95321825913403   R(theta=0) [km] 
11.70443553405696   R(theta=pi/2, phi=0) [km] 
10.85833050326065   R(theta=pi/2, phi=pi/2) [km] 
11.61554118466738   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
113    step
0    abscidia of the rotation axis [km] 
2117.020116303504    Orbital frequency Omega [rad/s] 
0.01205848832991352    Abscidia ``center of mass'' star 1 [km] 
0.01205848832991352    Abscidia ``center of mass'' star 2 [km] 
1.527238356050241   Baryon mass of star 1  [M_sol] 
1.192758028630921e-06   relative variation enth. star 1
10.95323040676193   R(theta=0) [km] 
11.70445086768114   R(theta=pi/2, phi=0) [km] 
10.85834248017256   R(theta=pi/2, phi=pi/2) [km] 
11.61555504517835   R(theta=pi/2, phi=pi) [km] 
1.527238356050241   Baryon mass of star 2  [M_sol] 
1.192758028630921e-06   relative variation enth. star 2
10.95323040676193   R(theta=0) [km] 
11.70445086768114   R(theta=pi/2, phi=0) [km] 
10.85834248017256   R(theta=pi/2, phi=pi/2) [km] 
11.61555504517835   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
114    step
0    abscidia of the rotation axis [km] 
2117.020468145555    Orbital frequency Omega [rad/s] 
0.0120589774503399    Abscidia ``center of mass'' star 1 [km] 
0.0120589774503399    Abscidia ``center of mass'' star 2 [km] 
1.527237525271353   Baryon mass of star 1  [M_sol] 
1.438155747374889e-06   relative variation enth. star 1
10.95323085613414   R(theta=0) [km] 
11.70444955481273   R(theta=pi/2, phi=0) [km] 
10.85834184172635   R(theta=pi/2, phi=pi/2) [km] 
11.61555655125542   R(theta=pi/2, phi=pi) [km] 
1.527237525271353   Baryon mass of star 2  [M_sol] 
1.438155747374889e-06   relative variation enth. star 2
10.95323085613414   R(theta=0) [km] 
11.70444955481273   R(theta=pi/2, phi=0) [km] 
10.85834184172635   R(theta=pi/2, phi=pi/2) [km] 
11.61555655125542   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
115    step
0    abscidia of the rotation axis [km] 
2117.020565549922    Orbital frequency Omega [rad/s] 
0.01205857208681937    Abscidia ``center of mass'' star 1 [km] 
0.01205857208681937    Abscidia ``center of mass'' star 2 [km] 
1.52723699596227   Baryon mass of star 1  [M_sol] 
1.446469533766425e-06   relative variation enth. star 1
10.95323153917257   R(theta=0) [km] 
11.70445076051753   R(theta=pi/2, phi=0) [km] 
10.85834210623935   R(theta=pi/2, phi=pi/2) [km] 
11.61555900919713   R(theta=pi/2, phi=pi) [km] 
1.52723699596227   Baryon mass of star 2  [M_sol] 
1.446469533766425e-06   relative variation enth. star 2
10.95323153917257   R(theta=0) [km] 
11.70445076051753   R(theta=pi/2, phi=0) [km] 
10.85834210623935   R(theta=pi/2, phi=pi/2) [km] 
11.61555900919713   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
116    step
0    abscidia of the rotation axis [km] 
2117.020293461639    Orbital frequency Omega [rad/s] 
0.01205837585019687    Abscidia ``center of mass'' star 1 [km] 
0.01205837585019687    Abscidia ``center of mass'' star 2 [km] 
1.527233288849416   Baryon mass of star 1  [M_sol] 
1.281992771891726e-06   relative variation enth. star 1
10.95323044845613   R(theta=0) [km] 
11.70445351587068   R(theta=pi/2, phi=0) [km] 
10.85834122443052   R(theta=pi/2, phi=pi/2) [km] 
11.6155593547299   R(theta=pi/2, phi=pi) [km] 
1.527233288849416   Baryon mass of star 2  [M_sol] 
1.281992771891726e-06   relative variation enth. star 2
10.95323044845613   R(theta=0) [km] 
11.70445351587068   R(theta=pi/2, phi=0) [km] 
10.85834122443052   R(theta=pi/2, phi=pi/2) [km] 
11.6155593547299   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
117    step
0    abscidia of the rotation axis [km] 
2117.020067756451    Orbital frequency Omega [rad/s] 
0.01205860444448925    Abscidia ``center of mass'' star 1 [km] 
0.01205860444448925    Abscidia ``center of mass'' star 2 [km] 
1.527236336321001   Baryon mass of star 1  [M_sol] 
6.972361640092605e-07   relative variation enth. star 1
10.95323843145921   R(theta=0) [km] 
11.70446299617652   R(theta=pi/2, phi=0) [km] 
10.85834904516701   R(theta=pi/2, phi=pi/2) [km] 
11.61556909794326   R(theta=pi/2, phi=pi) [km] 
1.527236336321001   Baryon mass of star 2  [M_sol] 
6.972361640092605e-07   relative variation enth. star 2
10.95323843145921   R(theta=0) [km] 
11.70446299617652   R(theta=pi/2, phi=0) [km] 
10.85834904516701   R(theta=pi/2, phi=pi/2) [km] 
11.61556909794326   R(theta=pi/2, phi=pi) [km] 
0   Relative difference between the baryon masses
  
