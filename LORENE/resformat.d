# Grid 1 : 4x33x21x20  R_out(l) [km] :  11.704 23.409 54.196 inf
#     VE(M)	         VE(GB)        VE(FUS)   
  -1.64927e-05   3.13534e-05   4.44428e-05
#      d [km]                d_G [km]            d/(a1 +a1')             f [Hz]             M_ADM [M_sol]        J [G M_sol^2/c]    
4.00000000000000e+01  3.99758826840791e+01  1.70874990219828e+00  3.36934208408178e+02  2.78572917099683e+00  7.52782676385754e+00
#     H_c(1)[c^2]         e_c(1)[rho_nuc]       M_B(1) [M_sol]         r_eq(1) [km]             a2/a1(1)	          a3/a1(1)	  
1.94017171919383e-01  3.83928059928348e+00  1.52723633632100e+00  1.17044629961765e+01  9.27710143448195e-01  9.35817254925517e-01
#     H_c(2)[c^2]         e_c(2)[rho_nuc]       M_B(2) [M_sol]         r_eq(2) [km]             a2/a1(2)	          a3/a1(2)	  
1.94017171919383e-01  3.83928059928348e+00  1.52723633632100e+00  1.17044629961765e+01  9.27710143448195e-01  9.35817254925517e-01
